<?php
header("Content-Type: text/html; charset=utf-8");
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
    <!--[if lt IE 9]>
    <script src="js/vendor/html5shiv.js"></script>
    <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Занятия - часть 2. PHP. Занятие 4. ООП</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/bootstrap-theme.css">
        <link rel="stylesheet" href="css/custom-styles.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/jquery-1.11.2.js" type="text/javascript"></script>
        <script src="js/vendor/bootstrap.js" type="text/javascript"></script>
        <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js" type="text/javascript"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
            your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <h2>Занятия - часть 2. PHP. Занятие 4. ООП</h2>
                </div>
            </div>
        </div>

        <div class="container main">
            <div class="row">
                <div class="col-sm-12  col-lg-4">
                    <div class="panel panel-primary panel-body left-menu" data-spy="affix" data-offset-top="85">
                        <ul class="nav nav-pills nav-stacked">
                            <!-- main menu links -->
                            <!-- HTML start -->
                            <li><a href="index.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 1. 23.01</a></li>
                            <li><a href="html01hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 1.</a></li>
                            <li><a href="html02.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 2</a></li>
                            <li><a href="html02hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 2</a></li>
                            <li><a href="html03.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 3</a></li>
                            <li><a href="html03hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 3</a></li>
                            <li><a href="html04.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 4</a></li>
                            <li><a href="html04hw.html"><span class="glyphicon glyphicon-home"></span>HTML5. Занятие 4. ДЗ</a></li>
                            <li><a href="html05.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 5</a></li>
                            <!-- HTML end -->
                            <li class="divider"><hr></li>
                            <!-- PHP start -->
                            <li><a href="php02.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 2+ДЗ. 24.01</a></li>
                            <li><a href="php03.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 3 + ДЗ.</a>
                            <li class="active"><a href="php04.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 4. Начало ООП</a></li>
                            <li><a href="php05.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 5. Продолжение ООП</a></li>
                            <li><a href="php05hw-forms.php"><span class="glyphicon glyphicon-home"></span> PHP. Занятие 5. ДЗ классы элементов форм</a></li>
                            <li><a href="php06.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 6. Работа с БД</a></li>
                            <li><a href="php06hw-sql.php"><span class="glyphicon glyphicon-home"></span> PHP. Занятие 6. Работа с БД - ДЗ</a></li>
                            <!-- PHP end -->
                            <li class="divider"><hr></li>
                            <li><a href="tutorial01.html">Tutorial. Shuffle Letters</a></li>
                            <!-- /main menu links -->
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12  col-lg-8">

                    <section class="panel panel-primary">
                        <?php
                        include './class/Circle.php';
                        include './class/Square.php';
                        include './class/Shape.php';

                        if (isset($_POST['draw-shape'])) {
                            if (filter_input(INPUT_POST, 'shape-select') == 'circle') {
                                $circle = new Circle();

                                $circle->setCircleRadius(filter_input(INPUT_POST, 'radius'));
                                $circle->setCircleColor(filter_input(INPUT_POST, 'shape-color'));
                                $circle->setCircleBorderColor(filter_input(INPUT_POST, 'shape-border-color'));
                                $div= '<div>radius: ' . filter_input(INPUT_POST, 'radius').'</div>' .
                                    Shape::circleDiv($circle);
                            }
                            if (filter_input(INPUT_POST, 'shape-select') == 'square') {
                                $square= new Square();

                                $square->setSquareWidth(filter_input(INPUT_POST, 'sqwidth'));
                                $square->setSquareHeight(filter_input(INPUT_POST, 'sqheight'));
                                $square->setSquareColor(filter_input(INPUT_POST, 'shape-color'));
                                $square->setSquareBorderColor(filter_input(INPUT_POST, 'shape-border-color'));
                                $div= '<div>height: ' . filter_input(INPUT_POST, 'sqheight') .
                                    ' width: '. filter_input(INPUT_POST, 'sqwidth') . '</div>' .
                                    Shape::squareDiv($square);
                            }

							
                        }
                        ?>
                        <div class="panel-heading">
                            <h3 class="panel-title">Д.З. квадрат и круг</h3>
                        </div>
                        <div class="panel-body">
                            <div class="col-lg-5">
                                <form id="shape-metrics" action="php04.php" method="post">
                                    <fieldset>
                                        <legend>Параметры фигуры</legend>
                                        <div class="form-group">
                                            <label for="shape-select">
                                                Выберите форму
                                            </label>
                                            <select id="shape-select" name="shape-select" class="form-control">
                                                <option value="circle">Круг</option>
                                                <option value="square">Квадрат</option>
                                            </select>

                                        </div>
                                        <div class="form-group">
                                            <label for="shape-color">
                                                Выберите цвет фона
                                            </label>
                                            <input type="color" name="shape-color" id="shape-color" class="form-control" value="#ffffff">
                                        </div>

                                        <div class="form-group">
                                            <label for="shape-color">
                                                Выберите цвет рамки
                                            </label>
                                            <input type="color" name="shape-border-color" id="shape-border-color" class="form-control">
                                        </div>

                                        <!-- Dynamically formatted input elements -->
                                        <div id="shape-sizes">

                                        </div>
                                    </fieldset>
                                    <input type="submit" class="btn btn-success" value="Нарисовать" id="draw-shape" name="draw-shape">
                                </form>
                            </div>
                            <div class="col-lg-7">
                                <div class="well well-sm">
                                <div class="alert alert-info" role="alert" style="text-align:center"><span >Фигура</span></div>
                                <?php echo $div; ?>
                                </div>
                            </div>
                        </div>
                    </section>

                    <section class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Мой первый класс</h3>
                        </div>
                        <div class="panel-body">
                            <?php
                            //классы принято выводить в отдельные файлы
                            include './class/Number.php';
                            include './class/Arifm.php';

                            //----------------------------------------------------пример
                            class myClass {

                                public $myVariable1;
                                private $myVariable2;

                                public function myMethod() {
                                    echo $this->myVariable1;
                                }

                                public function getMyVariable2() {//геттер - получающий значения свойства
                                    return $this->myVariable2;
                                }

                                public function setMyVariable2($myVariable2) {//сеттер - метод, устанавливающий значения свойства
                                    $this->myVariable2 = $myVariable2;
                                }

                            }

                            //------------------------------------------------------------

                            $obj1 = new Number(5);
                            echo '<p>Создали объект со значением 5: <code>' . $obj1->getNumber() . '</code></p>';
                            $obj1->setNumber(10);
                            echo '<p>Вызвали метод передачи нового значения: <code>' . $obj1->getNumber() . '</code></p>';

                            $obj2 = new Number();
                            echo '<p>Новый объект со значением по умолчанию: <code>' . $obj2->getNumber() . '</code></p>';

                            $summa = Arifm::add($obj1, $obj2); //обращаемся к статическому методу класса без создания объекта
                            echo '<p>Работа статического метода Arifm::add: <code>' . $summa . '</code></p>';

                            //константа существует у класса, а не объекта. Обращение к константе
                            echo '<p>Константа Arifm::PI: <code>' . Arifm::PI . '</code></p>';

                            $calc = new Arifm();
                            $calc->someMethod();
                            ?>
                        </div>
                    </section>

                    <section class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Калькулятор v.2</h3>
                        </div>
                        <div class="panel-body">
                            <?php
                            if (isset($_POST['calc'])) {
                                $operand1 = new Number;
                                $operand2 = new Number;
                                //http://www.regexlib.com/
                                $numreg = "/^[-]?[0-9]+\.[0-9]*$?$)/";
                                $operator = filter_input(INPUT_POST, 'operator');

                                /*
                                  //проверка допустимости операторов
                                  if (!preg_match($numreg, $num1) || !preg_match($numreg, $num2)) {
                                  $out = "<p class=\"bg-warning result\">Введено не число</p>";
                                  } elseif ($operator == "/" && $operand2 == "0") {
                                  $out = "<p class=\"bg-warning result\">Делить на 0 нельзя</p>";
                                  } //Проверки пройдены - считаем выражение
                                  else {
                                  $operand1->setNumber(filter_input(INPUT_POST, 'num1'));
                                  $operand2->setNumber(filter_input(INPUT_POST, 'num2'));
                                  $result = Arifm::$operator($operand1, $operand2);
                                  }
                                 */

                                $operand1->setNumber(filter_input(INPUT_POST, 'num1'));
                                $operand2->setNumber(filter_input(INPUT_POST, 'num2'));
                                $result = Arifm::$operator($operand1, $operand2);
                                $out = "<p class=\"bg-success result\">Результат выражения <b>" . $operand1->getNumber() . $operator . $operand2->getNumber() . "</b>: " . $result . "</p>";
                            }
                            ?>

                            <form class="form-inline" id="calc-form" action="php04.php" method="post">
                                <div class="form-group">
                                    <input type="text" class="form-control" id="operand-1" name="num1"
                                           value="<?php /* echo $operand1->getNumber(); */ ?>" size="5" required="">
                                </div>
                                <div class="form-group operators">
                                    <kbd>
                                        <label>
                                            <input type="radio" value="add" name="operator" id="operator-plus" class="form-control"
                                                   checked="">+
                                        </label>
                                    </kbd>
                                    <kbd>
                                        <label>
                                            <input type="radio" value="minus" name="operator" id="operator-minus"
                                                   class="form-control">-
                                        </label>
                                    </kbd>
                                    <kbd>
                                        <label>
                                            <input type="radio" value="incr" name="operator" id="operator-umn" class="form-control">*
                                        </label>
                                    </kbd>
                                    <kbd>
                                        <label>
                                            <input type="radio" value="razd" name="operator" id="operator-razd"
                                                   class="form-control">/
                                        </label>
                                    </kbd>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" id="operand-2" name="num2"
                                           value="<?php /* echo $operand2->getNumber(); */ ?>" size="5" required="">
                                </div>
                                <button type="text" id="calc" name="calc" class="btn btn-info">Посчитать</button>
                            </form>
                            <?php echo $out ?>

                        </div>
                    </section>

                </div>
            </div>

            <hr>

            <footer>
                <p>&copy; Company 2014</p>
            </footer>
        </div>
        <!-- /container -->
        <script src="js/shapes.js" type="text/javascript"></script>
    </body>
</html>


