<?php
header("Content-Type: text/html; charset=utf-8");
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <!--[if lt IE 9]>
    <script src="js/vendor/html5shiv.js"></script>
    <script src="js/vendor/respond.min.js"></script>
    <![endif]--> 
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>PHP. Занятие 2. Регулярные выражение. Массивы. Обработка формы с помощью РНР</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/bootstrap-theme.css">
        <link rel="stylesheet" href="css/custom-styles.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/jquery-1.11.2.js" type="text/javascript"></script>
        <script src="js/vendor/bootstrap.js" type="text/javascript"></script>
        <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                if ($('#stinfotable').html()) {
                    $("#student-form").hide('slow');
                };
                var ref = $('#refine-stud');

                if (ref.html()) {
                    ref.click(function () {
                        $("#student-form").show('slow');
                        ref.replaceWith('<hr>');
                        $('div.main').find('section.panel:first').find('h3').html('Исправить');
                    });
                };
                 var menu=$('.left-menu');
                $(document).scroll(function(){
                    if(($(document).scrollTop()>1522) && (menu.hasClass('affix'))){
                        menu.hide(300, function(){
                            menu.removeClass('affix').addClass('off-affix');
                        });
                    };
                    if(($(document).scrollTop()<1522) && (menu.hasClass('off-affix'))){
                        menu.removeClass('off-affix').addClass('affix').show(300);
                    };
                });
            });

        </script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <h2>Занятия - часть 2. PHP. Занятие 2. Регулярные выражение. Массивы.</h2>
                </div>
            </div>
        </div>

        <div class="container main clearfix">
            <div class="row">
                <div class="col-sm-12  col-lg-4">
                    <div class="panel panel-primary panel-body left-menu" data-spy="affix" data-offset-top="85">
                        <ul class="nav nav-pills nav-stacked">
                            <!-- main menu links -->
                            <!-- HTML start -->
                            <li><a href="index.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 1. 23.01</a></li>
                            <li><a href="html01hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 1.</a></li>
                            <li><a href="html02.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 2</a></li>
                            <li><a href="html02hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 2</a></li>
                            <li><a href="html03.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 3</a></li>
                            <li><a href="html03hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 3</a></li>
                            <li><a href="html04.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 4</a></li>
                            <li><a href="html04hw.html"><span class="glyphicon glyphicon-home"></span>HTML5. Занятие 4. ДЗ</a></li>
                            <li><a href="html05.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 5</a></li>
                            <!-- HTML end -->
                            <li class="divider"><hr></li>
                            <!-- PHP start -->
                            <li class="active"><a href="php02.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 2+ДЗ. 24.01</a></li>
                            <li><a href="php03.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 3 + ДЗ.</a>
                            <li><a href="php04.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 4. Начало ООП</a></li>
                            <li><a href="php05.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 5</a></li>
                            <li><a href="php05hw-forms.php"><span class="glyphicon glyphicon-home"></span> PHP. Занятие 5. ДЗ классы элементов форм</a></li>
                            <li><a href="php06.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 6. Работа с БД</a></li>
                            <li><a href="php06hw-sql.php"><span class="glyphicon glyphicon-home"></span> PHP. Занятие 6. Работа с БД - ДЗ</a></li>
                            <!-- PHP end -->
                            <li class="divider"><hr></li>
                            <li><a href="tutorial01.html">Tutorial. Shuffle Letters</a></li>
                            <!-- /main menu links -->
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12  col-lg-8">
                    <section class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Форма абитуриента. Обработка формы с помощью РНР</h3>
                        </div>
                        <div class="panel-body">

                            <?php
                            if (isset($_POST['subm'])) {
                                $_POST['subm']=false;
                                $ltime = array(
                                    "lt-empty" => "Не выбрано",
                                    "full-day" => "Дневной стационар",
                                    "full-evning"=>"Вечерний стационар",
                                    "half-morning"=>"Утренний полустационар",
                                    "half-day"=>"Дневной полустационар",
                                    "half-evning"=>"Вечерний полустационар",
                                    "webprog"=>"Веб-разработка",
                                    "design"=>"Дизайн"
                                );

                                $namepreg = '/[-A-Za-zА-Яа-я]/';

                                $form_data_name = array(
                                    "last_name" => filter_input(INPUT_POST, 'input1name'),
                                    "first_name" => filter_input(INPUT_POST, 'input2name'),
                                    "second_name" => filter_input(INPUT_POST, 'input3name')
                                );

                                if (preg_match($namepreg, $form_data_name['last_name'])) {
                                    $last_name = $form_data_name['last_name'];
                                } else {
                                    $last_name = '<p class="alert alert-danger" role="alert">Фамилия введена не верно. Допустимы только буквы русского или английского алфавита</p>';
                                }
                                if (preg_match($namepreg, $form_data_name['first_name'])) {
                                    $first_name = $form_data_name['first_name'];
                                } else {
                                    $first_name = '<p class="alert alert-danger" role="alert">Имя введено не верно. Допустимы только буквы русского или английского алфавита</p>';
                                }
                                if (preg_match($namepreg, $form_data_name['second_name']) || $form_data_name['second_name']=='') {
                                    $second_name = $form_data_name['second_name'];
                                } else {
                                    $second_name = '<p class="alert alert-danger" role="alert">Отчество введено не верно. Допустимы только буквы русского или английского алфавита</p>';
                                }

                                echo
                                '<table class="table table-hover" id="stinfotable">' .
                                '<tr><th  colspan="2">Ваши данные</th></tr>' .
                                '<tr><td>Фамилия:</td><td>' . $last_name . '</td></tr>' .
                                '<tr><td>Имя:</td><td>' . $first_name . '</td></tr>' .
                                '<tr><td>Отчество:</td><td>' . $second_name . '</td></tr>' .
                                '<tr><td>Телефон:</td><td>' . filter_input(INPUT_POST, 'inputtel') . '</td></tr>' .
                                '<tr><td>E-mail:</td><td>' . filter_input(INPUT_POST, 'inputemail') . '</td></tr>' .
                                '<tr><td>Адрес:</td><td>' . filter_input(INPUT_POST, 'inputaddr') . '</td></tr>' .
                                '<tr><td>Форма обучения:</td><td>' . $ltime[filter_input(INPUT_POST, 'learning-time')] . '</td></tr>' .
                                '<tr><td>Направление обучения:</td><td>' . $ltime[filter_input(INPUT_POST, 'learndirection')] . '</td></tr>'.
                                '<tr><td>Язык:</td><td>' . filter_input(INPUT_POST, 'languages') . '</td></tr>'.
                                '</table>' .
                                '<button type="button" name="refine-stud" id="refine-stud" class="btn btn-success">Показать форму</button>  ';
                            }
                            ?>


                            <form class="form-horizontal" id="student-form" action="php02.php" method="post">
                                <!--  Контактная информация -->
                                <fieldset>
                                    <legend>Контактная информация</legend>
                                    <div class="form-group" >
                                        <label for="input1name" class="col-sm-2 control-label">*Фамилия</label>
                                        <div class="col-lg-10">
                                            <input type="text" id="input1name" name="input1name" placeholder="Фамилия" class="form-control" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="input2name" class="col-sm-2 control-label">*Имя</label>
                                        <div class="col-lg-10">
                                            <input type="text" id="input2name" name="input2name" placeholder="Имя" class="form-control" required="">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="input3name" class="col-sm-2 control-label">Отчество</label>
                                        <div class="col-lg-10">
                                            <input type="text" id="input3name" name="input3name" placeholder="Отчество" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label for="inputphone" class="col-lg-4 control-label">*Телефон</label>
                                        <div class="col-lg-8">
                                            <input type="tel" id="inputtel" name="inputtel"  placeholder="Телефон" class="form-control" required="">
                                        </div>
                                    </div>
                                    <div class="form-group col-lg-6">
                                        <label for="inputemail" class="col-lg-3 control-label">Email</label>
                                        <div class="col-lg-9">
                                            <input type="email" id="inputemail" name="inputemail" placeholder="Email" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputaddress" class="col-lg-2 control-label"> Адрес</label>
                                        <div class="col-lg-10">
                                            <textarea id="inputaddr" name="inputaddr" placeholder="Адрес" class="form-control" rows="3"></textarea>
                                        </div>
                                    </div>
                                </fieldset>

                                <!--  Обучение -->    
                                <fieldset>
                                    <legend>Обучение</legend>
                                    <div class="form-group">
                                        <label for="learning-time" class="col-sm-3 control-label">Форма обучения </label>
                                        <div class="col-sm-9">
                                            <select class="form-control" id="learning-time" name="learning-time">
                                                <option value="lt-empty">- выберите -</option>
                                                <optgroup label="Стационар">
                                                    <option value="full-day">Дневной стационар</option>
                                                    <option value="full-evning">Вечерний стационар</option>
                                                </optgroup>
                                                <optgroup label="Полустационар">
                                                    <option value="half-morning">Утренний полустационар</option>
                                                    <option value="half-day">Дневной полустационар</option>
                                                    <option value="half-evning">Вечерний полустационар</option>
                                                </optgroup>
                                            </select>
                                            <span class="help-block">Форма обучения и удобное время суток.</span>
                                        </div>
                                    </div>
                                    <div class="form-group">                                       
                                        <label class="col-sm-4">Направление обучения: </label>
                                        <div class="col-sm-8">
                                            <label for="webprog">Программирование</label>
                                            <input type="radio" id="webprog" value="webprog" name="learndirection">
                                            <label for="softprog">Дизайн</label>
                                            <input type="radio" id="design" value="design" name="learndirection">
                                        </div>
                                    </div>
                                </fieldset>
                                <fieldset>
                                    <legend>Дополнительная информация</legend>
                                    <div class="form-group">   
                                        <label for="languages" class="col-sm-4 control-label">Иностранный язык</label>
                                        <div class="col-sm-8">
                                            <select name="languages" id="languages" class="form-control">
                                                <option value="english">Английский</option>
                                                <option value="german">Немецкий</option>
                                                <option value="france">Французский</option>                                        
                                            </select>
                                        </div>
                                    </div>
                                </fieldset>
                                <button type="submit" name="subm" class="btn btn-success">Отправить</button>                                    
                            </form>
                        </div>
                    </section>
                    <section class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Регулярные выражения</h3>
                        </div>
                        <div class="panel-body">
                            <p>Фридл. Регулярные выражения</p>
                            <p><code>/#[0-9a-f]{6}|[0-9a-f]{3}/</code> провeрка цвета - 6 или 3 знака</p>
                            <p><code>/zz#fffzx/</code> функция проверки цвета тоже работает</p>
                            <p><code>a@</code> После <b>a</b> перед <b>@</b> - это граница слова = \b</p>
                            <p><a href="http://php.net/manual/ru/function.preg-match.php">Функция <code>preg_match</code></a></p>
                            <p><b>Пример 1</b></p>
                            <?php
                            $subject = "abcdef";
                            $pattern = '/^def/';
                            preg_match($pattern, $subject, $matches, PREG_OFFSET_CAPTURE, 3);
                            print_r($matches);
                            ?>
                            <?php
                            $subject = "abcdef";
                            $pattern = '/^def/';
                            echo"<pre>";
                            preg_match($pattern, substr($subject, 3), $matches, PREG_OFFSET_CAPTURE);
                            print_r($matches);
                            echo"</pre>";
                            ?>
                            <p><b>Пример 2</b></p>
                            <?php
                            // Символ "i" после закрывающего ограничителя шаблона означает
                            // регистронезависимый поиск.
                            if (preg_match("/php/i", "PHP is the web scripting language of choice.")) {
                                echo "Вхождение найдено.";
                            } else {
                                echo "Вхождение не найдено.";
                            }
                            ?>
                            <p><b>Пример #3 Извлечение доменного имени из URL</b></p>
                            <?php
                            // Извлекаем имя хоста из URL
                            preg_match('@^(?:http://)?([^/]+)@i', "http://www.php.net/index.html", $matches);
                            $host = $matches[1];

                            // извлекаем две последние части имени хоста
                            preg_match('/[^.]+\.[^.]+$/', $host, $matches);
                            echo "доменное имя: {$matches[0]}\n";
                            ?>
                            <p><a href="http://php.net/manual/ru/function.preg-replace.php">Функция <code>preg_replace</code></a></p>
                            <p><b>Пример 1. Пример #1 Использование подмасок, за которыми следует цифра</b></p>
                            <?php
                            echo"<pre>";
                            $string = 'April 15, 2003';
                            $pattern = '/(\w+) (\d+), (\d+)/i';
                            $replacement = '${1}1,$3';
                            echo preg_replace($pattern, $replacement, $string);
                            echo"</pre>";                            
                            ?> 

                        </div>
                    </section>
                    <section class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Массивы</h3>
                        </div>
                        <div class="panel-body">
                            <?php
                            $juices["яблоко"]["зеленое"] = "хорошее";
                            echo"<pre>";
                            var_dump($juices);
                            print_r($juices);
                            echo"</pre>";

                            echo"<p>Вывод через <code>foreach</code></p>";
                            $user = array(
                                "login" => "Administrator",
                                "password" => "ThU58!kksd",
                                "email" => "admin@myhost.ua",
                                "status" => "superAdministrator"
                            );
                            foreach ($user as $key => $value) {
                                echo $key . ": " . $value . "<br />";
                            }
                            
                            
                            ?>
                        </div>
                    </section>

                </div>
            </div>
        </div>

        <hr>

        <footer>
            <?php
            echo "<pre>";
            var_dump($GLOBALS);
            print_r($GLOBALS);
            echo "</pre>";
            ?>
            <p>&copy; Company 2014</p>
        </footer>

    </body>
</html>
