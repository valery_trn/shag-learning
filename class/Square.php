<?php

class Square {
    private $width, $height, $color, $bordercolor;

    function __construct(){
        $this->width=200;
        $this->height=150;
        $this->color='#fff';
        $this->bordercolor='#000';
    }

    public function setSquareWidth($width){
        $this->width=$width;
    }

    public function setSquareHeight($height){
        $this->height=$height;
    }

    public function setSquareColor($color){
        $this->color=$color;
    }

    public function setSquareBorderColor($bordercolor){
        $this->bordercolor=$bordercolor;
    }

    public function getSquareParams(){
        return array(
            'width'=>$this->width,
            'height'=>$this->height,
            'color'=>$this->color,
            'bordercolor'=>$this->bordercolor
        );

    }

}