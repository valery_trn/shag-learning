<?php

/**
 * Description of Integer
 * класс-обертка над обыкновенным чиcлом
 * 
 * @author Таранец.Валерий
 */
class Number {
    private $number;

    function __construct($number = 0) {   //функция-конструктор. название конструктора всегда такое. срабатывает автоматически
        //можно задать значение по умолчанию
        $this->number = $number;
        //echo '<p>Конструктор отработал</p>';
    }

    public function setNumber($number) {
        $this->number = $number;
    }

    public function getNumber() {
        return $this->number;
    }

}


