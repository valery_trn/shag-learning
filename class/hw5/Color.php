<?php

class Color {

    private $red;
    private $green;
    private $blue;
    private $alfa;

    function __construct() {
        $this->red = 100;
        $this->green = 100;
        $this->blue = 100;
        $this->alfa = 1;
        echo 'construct color<br />';
    }
    
    function setColors($red,$green,$blue,$alfa){
        echo 'set new colors in Color <br />';
        $this->red=$red;
        $this->green=$green;
        $this->blue=$blue;
        $this->alfa=$alfa;
    }
    
    function getChennels() {
        echo 'run getChennels <br />';
        return array(
            'red' => $this->red,
            'green' => $this->green,
            'blue' => $this->blue,
            'alfa' => $this->alfa);
    }

}
