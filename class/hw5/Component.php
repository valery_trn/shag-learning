<?php
include 'Color.php';

abstract class Component{
	protected $color;
        private $width;
        private $height;
        
	abstract protected function render();
	
	public function setColor(Color $color){
		$this->color=$color;
                echo 'run setColor from "Component" <br />';
	}
	
	public function setWidth($width){
		$this->width=$width;
	}
	
	public function setHeight($height){
		$this->height=$height;
	}
}