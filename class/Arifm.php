<?php
class Arifm {
    /**
     * 
     * @param Integer $number1 число 1
     * @param Integer $number2 число 2
     * @return int
     * 
     * Функция суммирования
     * наш класс арифметических действий - статических методов для действий с объектами Integer
     */
    public static function add(Number $number1, Number $number2) { //желательно объявить тип передаваемого 
        //аргумента (объект нашего класса Integer)
        return $number1->getNumber() + $number2->getNumber();
    }
    
    public static function minus(Number $number1, Number $number2){
        return $number1->getNumber()-$number2->getNumber();
    }
    
    public static function incr(Number $number1, $number2){
        return $number1->getNumber()*$number2->getNumber();
    }

    //объявим константу
    const PI = 3.14;

    public function someMethod() {
        echo self::PI; //ссылка на самого себя - на класс
    }

}
