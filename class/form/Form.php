<?php

/**
 * Description of Form
 *
 * @author master
 */
class Form extends Element {

    private $method;
    private $action;
    private $elements = array();

    function __construct($id='') {
        parent::__construct($id);
        $this->tagname = 'form';
    }

    public function setFormMethod($method) {
        $this->method = $method;
    }

    public function getFormMethod() {
        return $this->method;
    }

    public function setFormAction($action) {
        $this->action = $action;
    }

    public function getFormAction() {
        return $this->action;
    }

    /**
     * 
     * @param Element $element is rendered html element or array of rendered elements
     */
    public function addFormElement($element) {
        if (is_string($element)) {
            $this->elements[] = $element;
        } elseif (is_array($element)) {
            foreach ($element as $elem) {
                $this->elements[] = $elem;
            }
        } else {
            $this->elements[] = '';
        }
    }
    
    /**
     * 
     * @return string html element <form ....>...</form>
     */
    public function render() {
        $form = '<form ';

        if ($this->getId()) {
            $form = $form . 'id="' . $this->getId() . '" ';
        }
        
        $form = $form . $this->RenderCSSClasses();
        
        if ($this->getFormMethod()) {
            $form = $form . 'method="' . $this->getFormMethod() . '" ';
        }
        if ($this->getFormAction()) {
            $form = $form . 'action="' . $this->getFormAction() . '" ';
        }
        $form.='>';
        foreach ($this->elements as $elem) {
            $form.=$elem;
        }
        return $form.='</form>';
    }

}
