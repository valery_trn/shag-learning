<?php


class Button extends Input {
    private $description;
    
    public function __construct($id = '', $name = '', $type='submit', $description='Submit') {
        parent::__construct($id, $name);
        $this->description=$description;
        $this->type=$type;                
    }
    
    public function render() {
        $id='';
        $name='';
        $disabled='';
        if (!empty($this->attr['name'])) {
            $name = ' name="' . $this->attr['name'] . '" ';
        }
        if ($this->getId()) {
            $id = ' id="' . $this->getId() . '" ';
        }
        if ($this->disabled){
            $disabled=' disabled';
        }
        return '<button type="' . $this->getType() . '" ' . $id . $name . 
                $this->renderCSSClasses() . $this->getAdditional() . $disabled . '>' .
                $this->description .'</button>';
    }
}
