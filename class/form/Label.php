<?php
/**
 * Created by PhpStorm.
 * User: master
 * Date: 19.02.2015
 * Time: 10:11
 */

class Label extends Element {
    private $for;
    private $text;

    function __construct($id = '', $for='', $text='')
    {
        parent::__construct($id);
        $this->for=$for;
        $this->text=$text;
        $this->tagname='label';
    }

    function setFor($for){
        $this->for=$for;
    }

    function setText($text){
        $this->text=$text;
    }


    function render(){
        return '<'. $this->tagname . $this->renderCSSClasses() . ' for="' . $this->for . '">' . $this->text .'</label>' . "\n";
    }
} 