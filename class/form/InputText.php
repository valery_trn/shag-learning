<?php

/**
 * Description of InputText
 *
 * @author vell
 */
class InputText extends Input{
    private $placeholder;
    private $maxlength;
    
    function __construct($id = '', $name = '', $placeholder='', $maxlength=0) {
        parent::__construct($id, $name);
        $this->setType('text');
        $this->placeholder=$placeholder;
        $this->maxlength=$maxlength;
    }

    function __clone()
    {
        $this->attr['id']='';
        $this->attr['name']='';
    }


    public function render() {
        $id='';
        $name='';
        $value='';
        $placeholder='';
        $maxlength='';
        $disabled='';
        if (!empty($this->attr['name'])) {
            $name = ' name="' . $this->attr['name'] . '" ';
        }
        if (!empty($this->attr['value'])) {
            $value = ' value="' . $this->attr['value'] . '" ';
        }
        if ($this->getId()) {
            $id = ' id="' . $this->getId() . '" ';
        }
        if (!empty($this->placeholder)){
            $placeholder=' placeholder="' . $this->placeholder . '" ';
        }
        if ($this->maxlength > 0){
            $maxlength=' maxlength="' . $this->maxlength . '" ';
        }
        if ($this->disabled){
            $disabled=' disabled';
        }
        return $this->getLabel() . '<input' . ' type="' . $this->getType() . '" ' . $id . $name . $value .
                $this->renderCSSClasses() . $this->getAdditional() . $placeholder . $maxlength. $disabled . '>';
    }
}
