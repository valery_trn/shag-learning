<?php
/**
 * Created by PhpStorm.
 * User: vell
 * Date: 19.02.2015
 * Time: 22:57
 */

class Checkbox extends Element{
    private $text;
    private $cheked=FALSE;

    function __construct($id = '', $name, $value, $text)
    {
        parent::__construct($id);
        $this->attr['name']=$name;
        $this->attr['value']=$value;
        $this->text=$text;
        $this->type='checkbox';
    }

    function setText($text){
        $this->text=$text;
    }

    function check(){
        $this->cheked=TRUE;
    }

    function uncheck(){
        $this->cheked=FALSE;
    }

    /**
     * @return string  html-element
     */
    function render()
    {
        $checked='';
        $id='';
        if($this->getId()){
            $id=' id="' . $this->getId() . '" ';
        }
        if($this->cheked){
            $checked='checked';
        }
        return '<label><input type="' . $this->type . '" ' . $id . $this->renderCSSClasses() .
        'name="'  . $this->attr['name'] .'" ' .  'value="' . $this->attr['value'] . '" '.
        $checked . '>' . $this->text . '</label>' . "\n";
    }
} 