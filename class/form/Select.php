<?php

class Select extends Element{
    private $options;
    private $form;
    public  $multiple;

    function __construct($id = '', $name='')
    {
        parent::__construct($id);
        $this->attr['name']=$name;
        $this->multiple=FALSE;
        $this->tagname='select';
    }

    function belongs_to($form=''){
        $this->form=$form;
    }

    /**
     * add existing Option ibject
     * @param Option $option 
     */
    function addOption(Option $option){
        $this->options[]=$option;
    }

    /**
     * Create and add new Option object
     * @param $value value of new option
     * @param $text description of new object
     */
    function createOption($value='',$description=''){
        $option=new Option($value, $description);
        $this->options[]=$option;
    }
    
    /**
     * return string html-element 'select' whis added options
     */
    function render(){
        $select='<' . $this->tagname;

        if($this->getId()){
            $select= $select .' id="' . $this->getId() . '" ';
        }

        if($this->attr['name']){
            $select= $select .' name="' . $this->attr['name'] . '" ';
        }

        $select.=$this->renderCSSClasses();

        if($this->form){
            $select=$select . ' form="'. $this->form;
        }

        if($this->multiple){
            $select=$select. ' multiple';
        }

        $select.=">\n";

        foreach ($this->options as $option) {
            $select.=$option->render();
        }

        $select.="</select>\n";
        echo  $select;
    }
} 