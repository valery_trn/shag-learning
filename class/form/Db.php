<?php


class Db {
    private $DBH;
    private $dbname;
    private $tables;

    function __construct($host,$dbname,$user,$pass)
    {
        #"mysql:host=$host;dbname=$dbname", $user, $pass
        $this->DBH=new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
        $this->dbname=$dbname;
        $STH = $this->DBH->query('show tables');
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        while($row = $STH->fetch()) {
            $this->tables[] = $row["Tables_in_".$dbname];
        }
        $this->DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING );
    }
    
    function getDbName(){
        return $this->dbname;
    }
    
    function getTableList(){
        return $this->tables;
    }


    function showTablesList(){
        echo '<pre>';
        echo 'Tables in database:'."\n";
        foreach($this->tables as $tablename){
            echo '<p>' . $tablename . '</p>';
        }
        echo '</pre>';
    }

    /**
     * SELECT . $selection . FROM . $table . WHERE . $clause
     * @param $selection
     * @param $table
     * @param string $clause
     * @return mixed
     */
    function getData($selection, $table, $clause=''){
        if($clause){
            $clause= " WHERE ".$clause;
        }
        //echo "<p>-------- <b>get Db->getData</b> ------- <br>". "SELECT ".$selection." FROM ".$table.$clause.'</p>';
        $STH=$this->DBH->query("SELECT ".$selection." FROM ".$table.$clause);
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        while($row = $STH->fetch()){
            foreach ($row as $key=>$data) {
                //echo '<p><b>' .$key. ':  </b>' .$data . '</p>';
                $fetch[$key][]=$data;
            }
            //echo '<hr />';
        }
        return $fetch;
    }

    function getTableKeys($table){
        $STH=$this->DBH->query("SELECT * FROM ".$table);
        $row=$STH->fetch();
        foreach ($row as $key => $data) {
            echo '<p>' . $key . '</p>';
            $tbkeys[]=$key;
        }
        return $tbkeys;
    }
    
    function getHandler(){
        return $this->DBH;
    }
}
