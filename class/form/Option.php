<?php
/**
 * Created by PhpStorm.
 * User: vell
 * Date: 19.02.2015
 * Time: 23:14
 */

class Option {
    private $value;
    private $description;

    function __construct($value='',$description=''){
        $this->value=$value;
        $this->description=$description;
    }

    function render(){
        return '<option value="' . $this->value . '">' . $this->description . '</option>' . "\n";
    }
} 