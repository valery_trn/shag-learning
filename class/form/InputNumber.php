<?php


class InputNumber extends Input {

    private $min;
    private $max;
    private $step;
    private $pattern;

    public function __construct($id = '', $name = '')
    {
        parent::__construct($id, $name);
        $this->setType('number');
    }


    public function setMinMaxStep($min, $max, $step){
        $this->min=$min;
        $this->max=$max;
        $this->step=$step;
    }

    public function setPattern ($pattern){
        $this->pattern=''.$pattern;
    }

    function render(){
        $id='';
        $name='';
        $value='';
        $min='';
        $max='';
        $step='';
        $pattern='';
        $disabled='';

        if (!empty($this->attr['name'])) {
            $name = ' name="' . $this->attr['name'] . '" ';
        }
        if (!empty($this->attr['value'])) {
            $value = ' value="' . $this->attr['value'] . '" ';
        }
        if ($this->getId()) {
            $id = ' id="' . $this->getId() . '" ';
        }
        if(!empty($this->min)){
            $min='min="' . $this->min . '"';
        }
        if(!empty($this->max)){
            $max='max="' . $this->max . '"';
        }
        if(!empty($this->step)){
            $step='step="' . $this->step . '"';
        }
        if(!empty($this->pattern)){
            $pattern='pattern="' . $this->pattern . '"';
        }
        if ($this->disabled){
            $disabled=' disabled';
        }
        
        return $this->getLabel() . '<input' . ' type="' . $this->getType() . '" ' . $id . $name . $value .
        $this->renderCSSClasses() . $this->getAdditional() . $min . $max . $step . $pattern . $disabled .'>';
    }

} 