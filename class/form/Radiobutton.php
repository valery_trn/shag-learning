<?php


class Radiobutton extends Element {
    private $text;
    private $cheked=FALSE;

    function __construct($id = '', $name, $value, $text)
    {
        parent::__construct($id);
        $this->attr['name']=$name;
        $this->attr['value']=$value;
        $this->text=$text;
        $this->type='radio';
    }

    function setText($text){
        $this->text=$text;
    }
    
    /*
     * set parameter 'checked'
     */
    function check(){
        $this->cheked=TRUE;
    }
    
    /*
     * set parameter 'unchecked
     */
    function uncheck(){
        $this->cheked=FALSE;
    }

    /**
     * return string html-element 
     * @return string
     */
    function render()
    {
        $checked='';
        $id='';
        $disabled='';
        if($this->getId()){
            $id=' id="' . $this->getId() . '" ';
        }
        if($this->cheked){
            $checked='checked';
        }
        if ($this->disabled){
            $disabled=' disabled';
        }
        return '<label><input type="' . $this->type . '" ' . $id . $this->renderCSSClasses() .
        'name="'  . $this->attr['name'] .'" ' .  'value="' . $this->attr['value'] . '" '.
        $checked . $disabled .'>' . $this->text . '</label>' . "\n";
    }


} 