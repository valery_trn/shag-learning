<?php

/**
 * Description of Element
 *
 * @author vell
 * @abstract set abstract function render for all elements
 */
abstract class Element implements renderCSSClasses, disable {

    protected $attr = array(
        'id'=>'',
        'name'=>'',
        'value'=>''
    );
    protected $cssclasses = array();
    protected $disabled;
    public $tagname;

    /**
     * 
     * return string  html-element
     */
    abstract function render();

    function __construct($id = '') {
        $this->attr['id'] = $id;
        $this->disabled=FALSE;
    }

    /**
     * $classes accept array of classes or string of classes separated by spaces
     * @param string 
     */
    public function setCSSClass($classes) {
        if (is_array($classes)) {
            foreach ($classes as $class) {
                $this->cssclasses[] = $class;
            }
        }
        if (is_string($classes)) {
            foreach (explode(' ', $classes) as $class) {
                $this->cssclasses[] = $class;
            }
        }
    }

    /**
     * return array of css classes
     * @return array
     */
    public function getCSSClass() {
        return $this->cssclasses;
    }

    public function getId() {
        return $this->attr['id'];
    }

    public function setId($id) {
        $this->attr['id'] = $id;
    }

    /**
     * set attribute 'disabled'
     */
    function disable(){
        $this->disabled=TRUE;
    }

    /**
     * delete attribute 'disabled'
     */
    function enable(){
        $this->disabled=FALSE;
    }

    /**
     * return string of css-classes class="class1 class2 ..."
     * @return string
     */
    final function renderCSSClasses() {        
        if (!empty($this->cssclasses)) {
            $cssclasses = ' class="'. implode(' ',$this->cssclasses) . '" ';
            return $cssclasses;
        } else {
            return '';
        }
    }

}
