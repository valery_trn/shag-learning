<?php

/**
 * Description of Textarea
 *
 * @author vell
 */
class Textarea extends Element{
    private $placeholder;
    private $rows=5;
    private $cols=50;

    function __construct($id = '', $name='', $rows, $cols, $placeholder)
    {
        parent::__construct($id);
        $this->attr['name']=$name;
        $this->tagname='textarea';
        $this->cols=$cols;
        $this->rows=$rows;
        $this->placeholder=$placeholder;
    }


    public function render(){
        $id='';
        $name='';
        $value='';
        $placeholder='';
        $disabled='';

        if (!empty($this->attr['name'])) {
            $name = ' name="' . $this->attr['name'] . '" ';
        }
        if (!empty($this->attr['value'])) {
            $value = ' value="' . $this->attr['value'] . '" ';
        }
        if ($this->getId()) {
            $id = ' id="' . $this->getId() . '" ';
        }
        if (!empty($this->placeholder)){
            $placeholder=' placeholder="' . $this->placeholder . '" ';
        }
        if ($this->disabled){
            $disabled=' disabled';
        }

        return '<' . $this->tagname . $id .$name . $value .
        $this->renderCSSClasses() . $placeholder .
        'cols="'. $this->cols . '" rows="' . $this->rows . $disabled. '"></textarea>';
    }
}
