<?php

/**
 * Description of Input
 *
 * @author vell
 */
class Input extends Element {

    /**
     * @var type type of input element
     * @var attr['value'] add value of input element
     * @var attr['name']  add name of input element
     * @var additional any additional attributes, like 'data-*, 'onclick' etc...
     */
    protected $type='';
    private $label='';
    private $additional='';
    
    public function __construct($id = '', $name='') {
        parent::__construct($id);
        $this->attr['name']=$name;
    }
    
    /*
     *  set parameters of input element type="..." name="..." value="..." and additional attributes
     */
    function setType($type){
        $this->type=$type;
    }
    
    function getType(){
        return $this->type;
    }
    
    /**
     * Add parameter 'value'
     * @param string $value
     */
    function setValue($value = '') {
        $this->attr['value'] = $value;
    }
    
    /**
     * Add parameter 'name'
     * @param string $name
     */
    function setName($name) {
        $this->attr['name'] = $name;
    }

    function setAdditional($additional = '') {
        $this->additional = $additional;
    }
    
    function getAdditional(){
        return $this->additional;
    }

    /**
     * Add label to input element
     * @param Label $label
     *
     */
    function addLabel(Label $label){
        $label->setFor($this->getId());
        $this->label= $label->render();
    }

    /**
     * Return rendered label element
     * @return string
     */
    function getLabel(){
        if(!empty($this->label)){
            return $this->label;
        } else {
            return '';
        }
    }

    /**
     * @return string html-element <input ...>
     */
    public function render() {
        $name='';
        $id='';
        $value='';
        if (!empty($this->attr['name'])) {
            $name = ' name="' . $this->attr['name'] . '" ';
        }
        if (!empty($this->attr['value'])) {
            $value = ' value="' . $this->attr['value'] . '" ';
        }
        if ($this->getId()) {
            $id = ' id="' . $this->getId() . '" ';
        }

        return $this->getLabel() . '<input' . ' type="' . $this->type . '" ' . $id . $name . $value .
                $this->renderCSSClasses() . $this->additional . '>';
    }

}
