<?php

class User extends TableEntry {

    private $name;
    private $email;
    private $pasword;
    private $user_type_id;
    private $user_type_name;

    public function __construct(\Db $base, $email) {
        parent::__construct($base);
        $this->email = $email;
        $this->setTable('users');

        //fetch user information
        $STH=$this->connection->query("SELECT name, password, user_type FROM users WHERE email='".$email."'");
        $STH->setFetchMode(PDO::FETCH_ASSOC);
        $data=$STH->fetch();
        $this->pasword= md5($data['password']);
        $this->name=$data['name'];
        $this->user_type_id=$data['user_type'];

        //fetch user_type
        $STH=$this->connection->query("SELECT user_types.type_name FROM users INNER JOIN user_types ON user_types.type_id = ".$this->user_type_id);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $this->user_type_name=$STH->fetch()->type_name;

    }

    /**
     * Create user record in database
     * returns only true or fals
     * TODO: add error-codes if user not created by different reasons
     * @param Db $db
     * @param $name
     * @param $email
     * @param $password
     * @return bool
     */
    public static function createUser(Db $db, $name, $email, $password) {
        //echo '<p>------ User->createUser <b>Try create new user </b> '.$name.'</p>';
        if (!TableEntry::recordExist($db, 'users', 'email', $email)) {
            $DBH = $db->getHandler();
            # Данные, которые надо вставить  
            $data = array(
                'user_id' => '',
                'name' => $name,
                'email' => $email,
                'password' => md5($password),
                'user_type' => '2'
            );

            # Сокращение  
            $STH = $DBH->prepare("INSERT INTO users (user_id, name, email, password, user_type) values (:user_id, :name, :email, :password, :user_type)");
            $STH->execute($data);
            #пользователь создан
            return true;
        } else {
            #пользователь не создан
            return false;
        };
    }

    public static function login(Db $db,$email,$password){
        if(TableEntry::recordExist($db,'users','email',$email)){

            $data=$db->getData('password','users',"email = '".$email."'");
            $data=$data['password'][0];
            //echo '<p>----------  <b>Check password </b> ----------<br />entered password: ';
            //print_r($data);
            //echo '</p>';
            if ($data==md5($password)){
                //echo '<p><b>logged in </b></p>';
                return 1;
            } else {
                //echo '<p><b>wrong password </b></p>';
                return 2;
            }
        } else {
            //echo '<p><b>name wrong </b></p>';
            return 3;
        }
    }

    /**
     * Change status member<-->deactivated
     * @param Db $db
     * @param $user_id
     * @param $new_type
     * @return bool
     */
    public static function changeUserType(Db $db, $user_id,$new_type){
        $DBH = $db->getHandler();
        $STH = $DBH->prepare("UPDATE users SET user_type=(:type) WHERE user_id=(:user_id)");
        $STH->bindParam(':type',$new_type);
        $STH->bindParam(':user_id',$user_id);
        $STH->execute();
        return '<p><span class="label label-success">Changed!</span></p>';
    }

    public function getPassword(){
        return $this->pasword;
    }

    public function getName(){
        return $this->name;
    }

    public function getTypeId(){
        return $this->user_type_id;
    }

    public function getTypeName(){
        return $this->user_type_name;
    }

}
