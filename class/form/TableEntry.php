<?php


class TableEntry {
    protected $connection;//ID of handler
    protected $table;
    
    public function __construct(Db $base) {
        $this->connection=$base->getHandler();
    }
    
    public function setTable($table){
        $this->table=$table;
    }
    
    static function recordExist(Db $connection, $table, $column, $record){
        //echo '<p><br /><br /><br />------- <b>check TableEntry::recordExist</b> '. $column .' '. $record .'------ </p>';
        $DBH=$connection->getHandler();
        $STH=$DBH->query("SELECT $column FROM $table");
        $STH->setFetchMode(PDO::FETCH_ASSOC);  
        while($row = $STH->fetch()){
            if ($row[$column]==$record){
                //echo '<p>record exist '.$row[$column].'</p>';
                return true;
            }
        }    
        //echo '<p>record NOT exist '.$row[$column].'</p>';
        return false;
    }
}
