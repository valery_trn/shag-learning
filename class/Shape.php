<?php

class Shape {

    public static function squareDiv(Square $square)
    {
        $params=$square->getSquareParams();
        return
            '<div class="shape-square" style="width:'.$params['width'].
            'px;height:'.$params['height'].
            'px;border:5px solid '.$params['bordercolor'].
            ';background:'.$params['color'].';"></div>';
    }

    public static function circleDiv(Circle $circle){
        $params=$circle->getCircleParams();
        return
            '<div class="shape-circle" style="width:'.($params['radius']*2).
            'px;height:'.($params['radius']*2).
            'px;border:5px solid '.$params['bordercolor'].
            ';background:'.$params['color'].';border-radius:50%"></div>';
    }
} 