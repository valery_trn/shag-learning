<?php

/**
 * Description of Component
 *
 * @author Таранец.Валерий
 */
abstract class Component {
    /**
     * @var Color $color
     */
    
    private $color;
    private $height;
    private $width;
    
    public function setColor($color){
        $this->color=$color;
    }    
    public function getColor(){
        return $this->color;
    }    
    public function setHeight($height){
        $this->height=$height;
    }
    public function getHeight(){
        return $this->height;
    }
    public function getWidth() {
        return $this->width;
    }
    public function setWidth($width){
        $this->width=$width;
    }
    
    abstract function render();
}
