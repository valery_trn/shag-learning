<?php


/**
 * Description of PositionedRectangle
 *
 * @author Таранец.Валерий
 */
class PositionedRectangle extends BorderRectangle {
    private $top;
    private $left;
    
    function __construct(Color $color, $width, $height, Color $borderColor, $top, $left) {
        parent::__construct($color, $width, $height, $borderColor);
        $this->top=$top;
        $this->left=$left;
    }
    
    public function render() {
        echo '<div style="background-color:rgb('.$this->getColor()->toString().'); width:'.$this->getWidth().'px;height:'.$this->getHeight().'px;border:5px solid rgb('.$this->borderColor->toString().
                ');position:absolute; top:'.$this->top.'px;left:'.$this->left.'px;"></div>';
    }
}
