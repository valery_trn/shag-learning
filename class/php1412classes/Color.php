<?php
/**
 * Description of Color
 *
 * @author Таранец.Валерий
 */
class Color {
    private $red;
    private $green;
    private $blue;
    
    public function __construct($red,$green,$blue) {
        $this->blue=$blue;
        $this->green=$green;
        $this->red=$red;
    }


    public function toString(){
        return $this->red.','.$this->green.','.$this->blue;
    }

}