<?php


/**
 * Description of BorderRectangle
 *
 * @author Таранец.Валерий
 */
class BorderRectangle extends Rectangle {
    protected $borderColor;
    
    public function __construct(Color $color, $width, $height, Color $borderColor) {
        parent::__construct($color, $width, $height);
        $this->borderColor=$borderColor;
    }
    
    public function render() {
        echo '<div style="background-color:rgb('.$this->getColor()->toString().'); width:'.$this->getWidth().'px;height:'.$this->getHeight().'px;border:5px solid rgb('.$this->borderColor->toString().');"></div>';
    }
}
