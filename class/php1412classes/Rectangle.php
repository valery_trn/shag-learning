<?php

/**
 * Description of Rectangle
 *
 * @author Таранец.Валерий
 */
class Rectangle extends Component {
    public function __construct(Color $color, $width, $height) {
        $this->setColor($color);
        $this->setWidth($width);
        $this->setHeight($height);
    }
    
    public function render() {
        echo '<div style="background-color:rgb('.$this->getColor()->toString().'); width:'.$this->getWidth().'px;height:'.$this->getHeight().'px;"></div>';
    }
}
