<?php

class Circle {
    private $radius, $color, $bordercolor;

    function __construct(){
        $this->radius=100;
        $this->color='#fff';
        $this->bordercolor='#000';
    }

    public function setCircleRadius($radius){
        $this->radius=$radius;
    }

    public function setCircleColor($color){
        $this->color=$color;
    }

    public function setCircleBorderColor($bordercolor){
        $this->bordercolor=$bordercolor;
    }

    public function getCircleParams(){
        return array(
            'radius'=>$this->radius,
            'color'=>$this->color,
            'bordercolor'=>$this->bordercolor
        );

    }

}