<?php
session_start();
header("Content-Type: text/html; charset=utf-8");
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
    <!--[if lt IE 9]>
    <script src="js/vendor/html5shiv.js"></script>
    <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Базы данных и PHP</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/bootstrap-theme.css">
        <link rel="stylesheet" href="css/custom-styles.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/jquery-1.11.2.js" type="text/javascript"></script>
        <script src="js/vendor/bootstrap.js" type="text/javascript"></script>
        <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js" type="text/javascript"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
            your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <h2>Занятия - часть 2. PHP. 6. Базы данных и PHP</h2>
                </div>
            </div>
        </div>

        <div class="container main">
            <div class="row">
                <div class="col-sm-12  col-lg-4">
                    <div class="panel panel-primary panel-body left-menu" data-spy="affix" data-offset-top="85">
                        <ul class="nav nav-pills nav-stacked">
                            <!-- main menu links -->
                            <!-- HTML start -->
                            <li><a href="index.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 1. 23.01</a></li>
                            <li><a href="html01hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 1.</a></li>
                            <li><a href="html02.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 2</a></li>
                            <li><a href="html02hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 2</a></li>
                            <li><a href="html03.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 3</a></li>
                            <li><a href="html03hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 3</a></li>
                            <li><a href="html04.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 4</a></li>
                            <li><a href="html04hw.html"><span class="glyphicon glyphicon-home"></span>HTML5. Занятие 4. ДЗ</a></li>
                            <li><a href="html05.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 5</a></li>
                            <!-- HTML end -->
                            <li class="divider"><hr></li>
                            <!-- PHP start -->
                            <li><a href="php02.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 2+ДЗ. 24.01</a></li>
                            <li><a href="php03.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 3 + ДЗ.</a>
                            <li><a href="php04.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 4. Начало ООП</a></li>
                            <li><a href="php05.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 5. Продолжение ООП</a></li>
                            <li><a href="php05hw-forms.php"><span class="glyphicon glyphicon-home"></span> PHP. Занятие 5. ДЗ классы элементов форм</a></li>
                            <li class="active"><a href="php06.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 6. Работа с БД</a></li>
                            <li><a href="php06hw-sql.php"><span class="glyphicon glyphicon-home"></span> PHP. Занятие 6. Работа с БД - ДЗ</a></li>
                            <!-- PHP end -->
                            <li class="divider"><hr></li>
                            <li><a href="tutorial01.html">Tutorial. Shuffle Letters</a></li>
                            <!-- /main menu links -->
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12  col-lg-8">
                    <section class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Базы данных и PHP. Работа с сессией</h3>
                        </div>
                        <div class="panel-body">
                            <p>По теме: <a href="http://habrahabr.ru/post/137664/">Почему стоит пользоваться PDO для работы с базой данных</a></p>
                            <p><a href="http://php.net/manual/ru/book.pdo.php">Объекты данных PHP</a></p>
                            <p><a href="http://megapuper.ru/mediawiki/index.php/%D0%A8%D0%BF%D0%B0%D1%80%D0%B3%D0%B0%D0%BB%D0%BA%D0%B0_%D0%BF%D0%BE_%D0%BA%D0%BE%D0%BC%D0%B0%D0%BD%D0%B4%D0%B0%D0%BC_MYSQL">Шпаргалка по командам MYSQL</a></p>

                            <p><a href="http://ruseller.com/lessons.php?rub=28&id=557">SQL для начинающих</a></p>

                            <p><a href="http://blog.ox2.ru/php/avtorizaciya-i-rabota-sessii/">Работа с сессиями. Создание простой авторизации на PHP</a></p>
                            <hr />
                            <p>INNER JOIN</p>
                            <pre>
select `works`.`name`, `works`.`email`,`position`.title 
FROM works 
left join `position` on `works`.`id_pos`=`position`.`id_pos`</pre>
                            <p>Выберет имя и мейл из таблицы работников и привяжет данные из таблицы должностей и возьмет названия должностей</p>
                            <p>LEFT JOIN</p>
                            <p>Выберет имя и мейл из таблицы работников и привяжет данные из таблицы должностей и возьмет названия должностей, если нет должности, то будет NULL</p>
                            <p>RIGHT JOIN</p>
                            <p>Выберет имя и мейл из таблицы работников и привяжет данные из таблицы должностей и возьмет названия должностей, также выведет те должности, которым не соответствует ни одного человека</p>
                            <?php
                            
                            // Соединяемся, выбираем базу данных
                            $link = mysql_connect('127.0.0.1', 'root', '')
                                    or die('Не удалось соединиться: ' . mysql_error());
                            echo 'Соединение успешно установлено';
                            mysql_select_db('organisation') or die('Не удалось выбрать базу данных');

                            // Выполняем SQL-запрос
                            $query = 'select `works`.`name`, `works`.`email`,`position`.title '
                                    . 'FROM works '
                                    . 'INNER JOIN `position` '
                                    . 'ON `works`.`id_pos`=`position`.`id_pos`';
                            $result = mysql_query($query) or die('Запрос не удался: ' . mysql_error());

                            // Выводим результаты в html
                            echo "<table class='table'>\n";
                            //первый запрос для получения ключей в шапку таблицы
                            $line = mysql_fetch_array($result, MYSQL_ASSOC);
                                echo "<tr>";
                                foreach ($line as $key=>$col_value) {
                                    echo "\t\t<th>$key</th>\n";
                                }
                                echo "\t</tr>\n";
                                echo "<tr>";
                                foreach ($line as $col_value) {
                                    echo "\t\t<td>$col_value</td>\n";
                                }
                                echo "\t</tr>\n";
                            
                            while ($line = mysql_fetch_array($result, MYSQL_ASSOC)) {
                                echo "<tr>";
                                foreach ($line as $col_value) {
                                    echo "\t\t<td>$col_value</td>\n";
                                }
                                echo "\t</tr>\n";
                            }
                            echo "</table>\n";

                            // Освобождаем память от результата
                            mysql_free_result($result);

                            // Закрываем соединение
                            mysql_close($link);
                            ?> 
                            
                            <?php
                            echo '<p>Работает session_start()</p>';
                            if(!isset($_SESSION['counter'])) $_SESSION['counter']=0;
                            echo    '<p>Вы обновили эту страницу '. $_SESSION['counter'] . ' раз</p>';
                            $_SESSION['counter']++;
                            echo "<p><a href=". $_SERVER['PHP_SELF'] .">Обновить</a></p>";
                            echo "<p><a href='disconnect.php'>Завершить / обнулить сессию</a></p>";
                            ?>
                        </div>
                    </section>
                </div>
            </div>

            <hr>

            <footer>
                <p>&copy; Company 2014</p>
            </footer>
        </div>
        <!-- /container -->
    </body>
</html>


