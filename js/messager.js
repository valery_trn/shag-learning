/* 
 * Messager
 */

(function ($){
    $(document).ready(function(){
        var box=$('.chatbox-input'),
            chat=$('.message-window'),
            userName='Толик',
            userPicSrc=box.find('.user-thumb img').attr('src'),
            submitbtn=box.find('button'),        
            text=box.find('textarea');          

        
        function formatMessage(name, userpic_scr, text){
            var messageHTML={
                elem_wrapper:$('<div class="chat-message clearfix"></div>'),
                elem_user_wrapper:$('<div class="message-author col-lg-2 col-sm-3 img-thumbnail"></div>'),
                elem_userpic:$('<div class="user-thumb"></div>'),
                elem_name:$('<div class="message-author-name"></div>'),
                elem_date:$('<div class="message-date"></div>'),
                elem_txt_wrapper:$('<div class="message-text-wrapper  col-lg-10 col-sm-9"></div>'),
                elem_txt:$('<div class="message-text"></div>')
            };
            
            //format text element
            messageHTML.elem_txt.append(text);
            messageHTML.elem_txt.appendTo(messageHTML.elem_txt_wrapper);
            
            //format userpic+name+date
            messageHTML.elem_userpic.html('<img src="'+userpic_scr+'" alt="">');
            messageHTML.elem_name.html(name);
            var d=new Date();
            messageHTML.elem_date.html(d.getDate()+'.'+(d.getMonth()+1)+'.'+d.getFullYear()+' - '+(d.getHours()+1)+'.'+(d.getMinutes()+1));
            
           // messageHTML.elem_date.append
            messageHTML.elem_user_wrapper.append(messageHTML.elem_userpic).append(messageHTML.elem_name).append(messageHTML.elem_date);
            
            messageHTML.elem_user_wrapper.appendTo(messageHTML.elem_wrapper);
            messageHTML.elem_txt_wrapper.appendTo(messageHTML.elem_wrapper);
            
            return messageHTML.elem_wrapper.css('display','none');
        }
        

        submitbtn.click(function(e){
            e.preventDefault();
            var new_message=formatMessage(userName, userPicSrc ,text.val());
            text.val('');
            chat.append(new_message);
            new_message.fadeIn(400);
        });
        
    });
})(jQuery);