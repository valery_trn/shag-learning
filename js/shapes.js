var currentShape = 'circle';

//класс с 3 интпутами-метриками и методом изменения атрибутов
function inputs() {
    this.sqHeight = document.createElement('input');
    this.sqWidth = document.createElement('input');
    this.cirRadius = document.createElement('input');

    this.changeAttr = function (shape, attr, attrvalue) {
        this[shape].setAttribute(attr, attrvalue);
        return this[shape];
    };
    

};

var     formInputs = new inputs;

//проставляем одинаковые атрибуты для инпутов-метрик
for (var key in formInputs) {
    if (typeof(formInputs[key]) !== 'function') {
        formInputs[key].setAttribute('type', 'range');
        formInputs[key].setAttribute('min', 30);
        formInputs[key].setAttribute('max', 350);
        formInputs[key].setAttribute('value', 200);
        formInputs[key].setAttribute('type', 'range');
        formInputs[key].setAttribute('class', 'form-control');
    }
};

//индивидуальные атрибуты для инпутов-метрик
formInputs.changeAttr('cirRadius', 'name', 'radius');
formInputs.changeAttr('sqWidth', 'name', 'sqwidth');
formInputs.changeAttr('sqHeight', 'name', 'sqheight');
formInputs.changeAttr('cirRadius', 'id', 'radius');
formInputs.changeAttr('sqWidth', 'id', 'sqwidth');
formInputs.changeAttr('sqHeight', 'id', 'sqheight');


//функция рендерит элемент инпут-метрику радиус круга
function drawCircleInput(){
    formInputs.changeAttr('cirRadius', 'max', '150');
    formInputs.changeAttr('cirRadius', 'value', '100');
    inputWrapper.insertAdjacentHTML("afterBegin",
            '<div class="form-group col-lg-12"><label for="shape-size">Выберите радиус</label><div class="input-group">');
    inputWrapper.querySelector('.input-group').appendChild(formInputs.cirRadius).insertAdjacentHTML("afterEnd",'<div class="input-group-addon">100</div></div></div>');

    document.forms['shape-metrics'].elements['radius'].oninput = function () {
        inputWrapper.querySelectorAll('.input-group-addon')[0].innerHTML=formInputs.cirRadius.value;
    }
}


//функция рендерит инпуты высоту и ширину прямоугольника
function drawSquareInput(){
    formInputs.changeAttr('sqHeight', 'value', '200');
    formInputs.changeAttr('sqHeight', 'max', '300');
    
    inputWrapper.insertAdjacentHTML("afterBegin",
            '<div class="form-group col-lg-12"><label for="shape-size">Выберите высоту</label><div class="input-group">');
    inputWrapper.querySelector('.input-group').appendChild(formInputs.sqHeight).insertAdjacentHTML("afterEnd",'<div class="input-group-addon">200</div></div></div>');
    
    inputWrapper.insertAdjacentHTML("beforeEnd",
            '<div class="form-group col-lg-12"><label for="shape-size">Выберите ширину</label><div class="input-group">');
    inputWrapper.querySelectorAll('.input-group')[1].appendChild(formInputs.sqWidth).insertAdjacentHTML("afterEnd",'<div class="input-group-addon">150</div></div></div>');

    document.forms['shape-metrics'].elements['sqheight'].oninput = function () {
        inputWrapper.querySelectorAll('.input-group-addon')[0].innerHTML=formInputs.sqHeight.value;
    }
    document.forms['shape-metrics'].elements['sqwidth'].oninput = function () {
        inputWrapper.querySelectorAll('.input-group-addon')[1].innerHTML=formInputs.sqWidth.value;
    }
}

var     inputWrapper=document.getElementById('shape-sizes'),//wrapper для инпутов
        selectShape=document.forms['shape-metrics'].elements['shape-select'];//select


drawCircleInput();//по умолчанию выбран круг

//проверка какая фигура выбрана при выборе селекта
//отрисовка инпутов
selectShape.onchange=function(){

    console.log(selectShape.value);
    if(selectShape.value==='circle'&&currentShape==='square'){
        inputWrapper.innerHTML='';
        currentShape='circle';
        drawCircleInput();

    } else if(selectShape.value==='square'&&currentShape==='circle') {
        inputWrapper.innerHTML = '';
        currentShape = 'square';
        drawSquareInput();

    }
};


