<?php
header("Content-Type: text/html; charset=utf-8");
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<!--[if lt IE 9]>
<script src="js/vendor/html5shiv.js"></script>
<script src="js/vendor/respond.min.js"></script>
<![endif]-->
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/bootstrap-theme.css">
    <link rel="stylesheet" href="css/custom-styles.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/jquery-1.11.2.js" type="text/javascript"></script>
    <script src="js/vendor/bootstrap.js" type="text/javascript"></script>
    <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js" type="text/javascript"></script>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <h2>Занятия - часть 2. PHP. 3</h2>
        </div>
    </div>
</div>

<div class="container main">
    <div class="row">
        <div class="col-sm-12  col-lg-4">
            <div class="panel panel-primary panel-body left-menu" data-spy="affix" data-offset-top="85">
                <ul class="nav nav-pills nav-stacked">
                    <!-- main menu links -->
                    <!-- HTML start -->
                    <li><a href="index.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 1. 23.01</a></li>
                    <li><a href="html01hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 1.</a></li>
                    <li><a href="html02.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 2</a></li>
                    <li><a href="html02hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 2</a></li>
                    <li><a href="html03.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 3</a></li>
                    <li><a href="html03hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 3</a></li>
                    <li><a href="html04.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 4</a></li>
                    <li><a href="html04hw.html"><span class="glyphicon glyphicon-home"></span>HTML5. Занятие 4. ДЗ</a></li>
                    <li><a href="html05.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 5</a></li>
                    <!-- HTML end -->
                    <li class="divider"><hr></li>
                    <!-- PHP start -->
                    <li><a href="php02.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 2+ДЗ. 24.01</a></li>
                    <li class="active"><a href="php03.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 3 + ДЗ.</a>
                    <li><a href="php04.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 4. Начало ООП</a></li>
                    <li><a href="php05.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 5. Продолжение ООП</a></li>
                    <li><a href="php05hw-forms.php"><span class="glyphicon glyphicon-home"></span> PHP. Занятие 5. ДЗ классы элементов форм</a></li>
                    <li><a href="php06.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 6. Работа с БД</a></li>
                    <li><a href="php06hw-sql.php"><span class="glyphicon glyphicon-home"></span> PHP. Занятие 6. Работа с БД - ДЗ</a></li>
                    <!-- PHP end -->
                    <li class="divider"><hr></li>
                    <li><a href="tutorial01.html">Tutorial. Shuffle Letters</a></li>
                    <!-- /main menu links -->
                </ul>
            </div>
        </div>
        <div class="col-sm-12  col-lg-8">
            <section class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Проводник</h3>
                </div>
                <div class="panel-body">
                    <div class="panel panel-default">

                        <div class="panel-heading"><samp>
                        <?php
                        parse_str($_SERVER['QUERY_STRING']);
                        if ($newdir) {
                            chdir($newdir);
                        }

                        $curDir = getcwd();
                        echo $curDir . '</samp></div><div class="panel-body"><samp>';
                        $dirFiles = scandir($curDir);
                        
                        //сортируем сначала папки, потом файлы
                        foreach ($dirFiles as $node){
                            if (is_dir($curDir . "\\" . $node)) $dirFilesSort[]=$node;
                        }
                        foreach ($dirFiles as $node){
                            if (is_file($curDir . "\\" . $node)) $dirFilesSort[]=$node;
                        }
                        
                        //формируем таблицу со ссылками
                        $dirTable = "\n<table class=\"table table-condensed table-hover\">\n";
                        foreach ($dirFilesSort as $node) {
                            if (is_dir($curDir . "\\" . $node)) {
                                $link = '<a href="?newdir=' . $curDir . '\\' . $node . '">' .
                                    "<span class=\"glyphicon glyphicon-folder-open\" aria-hidden=\"true\"></span>&nbsp;&nbsp;" . $node . "</a>";
                            }
                            if (is_file($curDir . "\\" . $node)) {
                                $link = "<span class=\"glyphicon glyphicon-file text-primary\" aria-hidden=\"true\"></span>&nbsp;&nbsp;" . $node;
                            }
                            if ($node == ".." || $node == ".") {
                                $link = '<a href="?newdir=' . $curDir . '\\' . $node . '">' . $node . "</a>";
                            };

                            $dirTable = $dirTable . "<tr><td>" . $link . "</tr></td>\n";
                        }
                        $dirTable .= "</table>";

                        echo $dirTable;
                        ?>
                        </div>
                        </samp>
                    </div>
                </div>
            </section>
            <?php
            $operand1 = 0;
            $operand2 = 0;
            if (isset($_POST['calc'])) {
                $operators = array(
                    "plus" => "+",
                    "minus" => "-",
                    "umn" => "*",
                    "razd" => "/"
                );
                $operand1 = filter_input(INPUT_POST, 'operand-1');
                $operand2 = filter_input(INPUT_POST, 'operand-2');
                $operator = $operators[filter_input(INPUT_POST, 'operator')];

                //http://www.regexlib.com/
                $numreg = "/[-]?([0-9]+\.[0-9]*)|([0-9]*\.[0-9]+)|([0-9]+)/";

                //проверка допустимости операторов
                if (!preg_match($numreg, $operand1) || !preg_match($numreg, $operand2)) {
                    $result = "<p class=\"bg-warning result\">Введено не число</p>";
                } elseif ($operator == "/" && $operand2 == "0") {
                    $result = "<p class=\"bg-warning result\">Делить на 0 нельзя</p>";
                } //Проверки пройдены - считаем выражение
                else {
                    switch (filter_input(INPUT_POST, 'operator')) {
                        case "plus":
                            $result = $operand1 + $operand2;
                            break;
                        case "minus":
                            $result = $operand1 - $operand2;
                            break;
                        case "umn":
                            $result = $operand1 * $operand2;
                            break;
                        case "razd":
                            $result = $operand1 / $operand2;
                    }
                    $result = "<p class=\"bg-success result\">Результат выражения <b>" . $operand1 . $operator . $operand2 . "</b>: " . $result . "</p>";
                }
            }
            ?>
            <section class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Калькулятор</h3>
                </div>
                <div class="panel-body">
                    <form class="form-inline" id="calc-form" action="php03.php" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" id="operand-1" name="operand-1"
                                   value="<?php echo $operand1 ?>" size="5" required="">
                        </div>
                        <div class="form-group operators">
                            <label>
                                <input type="radio" value="plus" name="operator" id="operator-plus" class="form-control"
                                       checked="">+
                            </label>
                            <label>
                                <input type="radio" value="minus" name="operator" id="operator-minus"
                                       class="form-control">-
                            </label>
                            <label>
                                <input type="radio" value="umn" name="operator" id="operator-umn" class="form-control">*
                            </label>
                            <label>
                                <input type="radio" value="razd" name="operator" id="operator-razd"
                                       class="form-control">/
                            </label>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control" id="operand-2" name="operand-2"
                                   value="<?php echo $operand2 ?>" size="5" required="">
                        </div>
                        <button type="text" id="calc" name="calc" class="btn btn-info">Посчитать</button>
                    </form>
                    <?php echo $result ?>
                </div>
            </section>
            <section class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Парсим магазин</h3>
                </div>
                <div class="panel-body">
                    <?php
                    $html = file_get_contents("tovars.htm");

                    //у таблицы с изображением товара есть класс MainProdTbl, ищем картинки
                    $item_img_pattern = "#class=\"MainProdTbl\".*TABLE>#mUsi";
                    preg_match_all($item_img_pattern, $html, $parsed_img);
                    //вычищаем лишние теги и собираем новый массив
                    foreach ($parsed_img[0] as $item) {
                        $item = preg_replace("#class=\"MainProdTbl\">(\s*)<TBODY>(\s*)<TR>(\s*)<TD>(\s*)<A .*\">#mUsi", " ", $item);
                        $item = preg_replace("#</TD>(\s*)<TD>(\s*)</TR>(\s*)</TBODY>(\s*)</TABLE>#mUsi", " ", $item);
                        $items_img[] = $item;
                    }

                    //ищем названия и цены по классу sclc
                    $name_price_pattern = "#class=\"sclc\".*span><br>(\s*)<b>.*</b>#mUsi";
                    preg_match_all($name_price_pattern, $html, $parsed_name_price);
                    //вычищаем лишние теги и собираем новый массив
                    foreach ($parsed_name_price[0] as $item) {
                        $item = preg_replace("#class=\"sclc\"><A .*\">#mUsi", " ", $item);
                        $item = str_replace("</A></SPAN><BR>", " ", $item);
                        $items_name_price[] = $item;
                    }

                    //склеиваем результат в таблицу
                    $result_table = "\n<table class=\"table\">\n<tr><th>Цена и название</th><th>Изображение</th></tr>\n";
                    for ($i = 0; $i < count($items_name_price); $i++) {
                        $result_table = $result_table . "<tr><td>" . $items_name_price[$i] . "</td><td>" . $items_img[$i] . "</td></tr>\n";
                    }
                    $result_table .= "\n</table>\n";
                    echo $result_table;
                    ?>
                </div>
            </section>

        </div>
    </div>

    <hr>

    <footer>
        <p>&copy; Company 2014</p>
    </footer>
</div>
<!-- /container -->
</body>
</html>


