'use strict';

module.exports = function (grunt) {

  grunt.initConfig({
      autoprefixer: {
      options: {
        browsers: [
          'Android 2.3',
          'Android >= 4',
          'Chrome >= 20',
          'Firefox >= 24', // Firefox 24 is the latest ESR
          'Explorer >= 8',
          'iOS >= 6',
          'Opera >= 12',
          'Safari >= 6'
        ]
      },
      core: {
        options: {
          map: false
        },
        src: 'css/*.css'
      }
    }
  });

  grunt.loadNpmTasks('grunt-autoprefixer');

  grunt.registerTask('build', [
    'autoprefixer:core'
  ]);

};
