<?php
header("Content-Type: text/html; charset=utf-8");
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
    <!--[if lt IE 9]>
    <script src="js/vendor/html5shiv.js"></script>
    <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>PHP. Продолжение ООП</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/bootstrap-theme.css">
        <link rel="stylesheet" href="css/custom-styles.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/jquery-1.11.2.js" type="text/javascript"></script>
        <script src="js/vendor/bootstrap.js" type="text/javascript"></script>
        <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js" type="text/javascript"></script>
    </head>
    <body>
        <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
            your browser</a> to improve your experience.</p>
        <![endif]-->
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <h2>Занятия - часть 2. PHP. 5. Продолжение ООП</h2>
                </div>
            </div>
        </div>

        <div class="container main">
            <div class="row">
                <div class="col-sm-12  col-lg-4">
                    <div class="panel panel-primary panel-body left-menu" data-spy="affix" data-offset-top="85">
                        <ul class="nav nav-pills nav-stacked">
                            <!-- main menu links -->
                            <!-- HTML start -->
                            <li><a href="index.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 1. 23.01</a></li>
                            <li><a href="html01hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 1.</a></li>
                            <li><a href="html02.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 2</a></li>
                            <li><a href="html02hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 2</a></li>
                            <li><a href="html03.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 3</a></li>
                            <li><a href="html03hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 3</a></li>
                            <li><a href="html04.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 4</a></li>
                            <li><a href="html04hw.html"><span class="glyphicon glyphicon-home"></span>HTML5. Занятие 4. ДЗ</a></li>
                            <li><a href="html05.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 5</a></li>
                            <!-- HTML end -->
                            <li class="divider"><hr></li>
                            <!-- PHP start -->
                            <li><a href="php02.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 2+ДЗ. 24.01</a></li>
                            <li><a href="php03.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 3 + ДЗ.</a>
                            <li><a href="php04.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 4. Начало ООП</a></li>
                            <li class="active"><a href="php05.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 5. Продолжение ООП</a></li>
                            <li><a href="php05hw-forms.php"><span class="glyphicon glyphicon-home"></span> PHP. Занятие 5. ДЗ классы элементов форм</a></li>
                            <li><a href="php06.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 6. Работа с БД</a></li>
                            <li><a href="php06hw-sql.php"><span class="glyphicon glyphicon-home"></span> PHP. Занятие 6. Работа с БД - ДЗ</a></li>
                            <!-- PHP end -->
                            <li class="divider"><hr></li>
                            <li><a href="tutorial01.html">Tutorial. Shuffle Letters</a></li>
                            <!-- /main menu links -->
                        </ul>
                    </div>
                </div>
                <div class="col-sm-12  col-lg-8">
                    <section class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Начало БД</h3>
                        </div>
                        <div class="panel-body">
                            
                        </div>
                    </section>
                    <section class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Абстрактный класс, наследование, дополненные конструкторы, наследование  2 уровня (Rectangle вариант C.A.)</h3>
                        </div>
                        <div class="panel-body" style="position: relative">
                                    <?php
                                    //spl_autoload_register подключает все файлы из указанной папки
                                    spl_autoload_register(function($class) {
                                        include './class/php1412classes/' . $class . '.php';
                                    });
                                    $color = new Color(22, 44, 77);
                                    $rect = new Rectangle($color, 200, 150);
                                    $rect->render();

                                    $newcolor = new Color(123, 200, 166);
                                    $bordcolor = new Color(133, 18, 22);
                                    $bordrect = new BorderRectangle($newcolor, 150, 200, $bordcolor);
                                    $bordrect->render();

                                    $fixedrect = new PositionedRectangle($color, 50, 80, $newcolor, 200, 420);
                                    $fixedrect->render();
                                    ?>
                        </div>
                    </section>
                    <section class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Абстрактный класс, наследование (Rectangle мой вариант)</h3>
                        </div>
                        <div class="panel-body">
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    Параметры прямоугольника:
                                </div>
                                <div class="panel-body">
                                    <form class="form-inline">
                                        <div class="form-group">
                                            <label for="rchennel">R</label>
                                            <input type="number" class="form-control" id="rchennel" placeholder="red 0...255" min="0" max="255">
                                        </div>
                                        <div class="form-group">
                                            <label for="gchennel">G</label>
                                            <input type="number" class="form-control" id="gchennel" placeholder="green 0...255" min="0" max="255">
                                        </div>
                                        <div class="form-group">
                                            <label for="gchennel">B</label>
                                            <input type="number" class="form-control" id="bchennel" placeholder="blue 0...255" min="0" max="255">
                                        </div>
                                        <div class="form-group">
                                            <label for="gchennel">Alfa</label>
                                            <input type="text" class="form-control" id="achennel" placeholder="alfa 0...1">
                                        </div>
                                        <button type="submit" class="btn btn-default">Set color</button>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <?php
                                /*
                                  echo '<p>';
                                  include './class/hw5/Rectangle.php';//вариант, сделанный мной дома
                                  $rectangle = new Rectangle();
                                  echo 'new $rectangle <br />';
                                  $color = new Color();
                                  echo 'new $color <br />';
                                  $color->setColors(130, 60, 200, 0.4);
                                  $rectangle->setColor($color);
                                  echo '</p>';
                                 */
                                ?>
                            </div>
                            <div class="col-lg-6">
                                <?php //$rectangle->render(); ?>
                            </div>
                        </div>
                    </section>
                    <section class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">ООП</h3>
                        </div>
                        <div class="panel-body">
                            <p>
                                <?php

                                class SimpleClass {

                                    public $var = ' значение по умолчанию <br />';

                                    public function displayVar() {
                                        echo $this->var;
                                    }

                                    //магические методы
                                    function __clone() {
                                        $this->var = 'просто строка переопределена магическим методом _clone <br />';
                                    }

                                }

                                $obj1 = new SimpleClass();
                                $obj2 = $obj1;
                                $obj1->var = ' новое значение скопировалось в оба объекта<br />'; //поменялось знчение в обоих объектах
                                $obj1->displayVar();
                                $obj2->displayVar();
                                $obj3 = clone $obj1; //клонирование - теперь это 2 независимых объекта
                                $obj1->var = ' СУПЕРНОВОЕ ЗНАЧЕНИЕ <br />';
                                $obj1->displayVar();
                                $obj2->displayVar();
                                $obj3->displayVar();
                                echo '<hr>';

                                //наследование 
                                //родительский класс
                                class ParentClass {

                                    private $privateVar;
                                    public $publicVar;
                                    protected $protectedVar = 123;

                                    public function someMethod() {
                                        echo 'Call parent someMethod <br />';
                                    }

                                }

                                //класс-наследник
                                class ChildClass extends ParentClass {

                                    public function childSomeMethod() {
                                        echo 'Call child someMethod <br />';
                                        echo 'значение protectedVar доступно в наследнике: ' . $this->protectedVar . '<br>';
                                    }

                                    //перегружаем метод, наследованный от родителя
                                    public function someMethod() {
                                        parent::someMethod(); //если нужно вызвать не перегруженный метод
                                        echo 'Call reloaded child someMethod </br>';
                                    }

                                }

                                $obj = new ChildClass();
                                $obj->childSomeMethod();
                                $obj->someMethod();

                                $obj->publicVar = 10;
                                echo 'Поменяли publicVar: ' . $obj->publicVar . '<br />';
                                //echo $obj->protectedVar=20; //выдаст Fatal error
                                ?>
                            </p>
                        </div>
                </div>
                </section>

            </div>
        </div>

        <hr>

        <footer>
            <p>&copy; Company 2014</p>
        </footer>
    </div>
    <!-- /container -->
</body>
</html>


