<?php
session_start();
header("Content-Type: text/html; charset=utf-8");
spl_autoload_register(function ($class) {
    include './class/form/' . $class . '.php';
});

//создаем соединение с БД
$dbconnect = new Db('localhost', 'shagbase', 'root', '');

//логин
//проверяем данные из мoдальной формы логина
if (isset($_POST['login-btn']) && filter_input(INPUT_POST, 'email') && filter_input(INPUT_POST, 'password')) {
    $user_email = filter_input(INPUT_POST, 'email');
    $user_password = filter_input(INPUT_POST, 'password');


    #вернется 1, если проверка пройдена и пользователь залогинен
    $login_status = User::login($dbconnect, $user_email, $user_password);

    #сессия
    if ($login_status == 1) {
        $_SESSION['userlogged'] = 1;
        $user_data = $dbconnect->getData('name, password', 'users', "email = '" . $user_email . "'");
        $user_name = $user_data['name'][0];
        $_SESSION['useremail'] = $user_email;
        $_SESSION['username'] = $user_name;
        $_SESSION['userpassword'] = $user_data['password'][0];
    }
    unset($_POST);
    header('Location: php06hw-sql.php', true, 303);
}

//создаем объект current user с использованием $_SESSION['useremail'] для доступа к параметрам пользователя
//$currentUser->user_type_id определяет права доступа
if ($_SESSION['userlogged']==1 && isset($_SESSION['useremail'])) {
    $currentUser = new User($dbconnect, $_SESSION['useremail']);
}


//регистрация
if (isset($_POST['registration-btn'])) {
    $reg_name = filter_input(INPUT_POST, 'regname');
    $reg_email = filter_input(INPUT_POST, 'regemail');
    $reg_password = filter_input(INPUT_POST, 'regpassword');
    if (strlen($reg_password) > 3) {
        User::createUser($dbconnect, $reg_name, $reg_email, $reg_password);
        $_SESSION['username'] = $reg_name;
        $_SESSION['new_user'] = true;
    } else {
        echo "Пароль слишком короткий";
    }
    unset($_POST);
    header('Location: php06hw-sql.php?new_user=true', true, 303);
}
?>

<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
    <!--[if lt IE 9]>
    <script src="js/vendor/html5shiv.js"></script>
    <script src="js/vendor/respond.min.js"></script>
    <![endif]-->
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>Базы данных и PHP. ДЗ</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="css/bootstrap.css">
        <link rel="stylesheet" href="css/bootstrap-theme.css">
        <link rel="stylesheet" href="css/custom-styles.css">
        <link rel="stylesheet" href="css/main.css">
        <script src="js/vendor/jquery-1.11.2.js" type="text/javascript"></script>
        <script src="js/vendor/bootstrap.js" type="text/javascript"></script>
        <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function(){
                    $('[data-toggle="tooltip"]').tooltip()
            });
        </script>
    </head>
    <body>
        <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
            your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- navbar  -->
        <div class="navbar navbar-default navbar-fixed-top" role="navigation">
            <nav class="container">
                <div class="navbar-header">
                    <h2>Занятия - часть 2. PHP. Работа с БД. ДЗ</h2>

                    <?php if (!$_SESSION['userlogged'] or ! isset($_SESSION['userlogged'])): ?>
                        <div class="nav navbar-nav navbar-right">
                            <button type="button" class="btn btn-info navbar-btn" data-toggle="modal"
                                    data-target="#login-modal">Войти
                            </button>
                            <button type="button" class="btn btn-info navbar-btn" data-toggle="modal"
                                    data-target="#registration-modal">Регистрация
                            </button>
                        </div>
                    <?php endif; ?>

                    <?php if (isset($_SESSION['userlogged']) && $_SESSION['userlogged']): ?>
                        <div class="nav navbar-nav navbar-right">
                            <a class="btn btn-info" href="disconnect.php">Выйти</a>
                        </div>

                        <div class="nav navbar-nav navbar-right">
                            <div><b><?php echo $currentUser->getName(); ?></b></div>
                            <div><?php echo $currentUser->getTypeName(); ?></div>
                        </div>
                    <?php endif; ?>

                </div>
            </nav>
        </div>


        <div class="container main">
            <div class="row">
                <!-- main menu -->
                <div class="col-sm-12  col-lg-4">
                    <div class="panel panel-primary panel-body left-menu" data-spy="affix" data-offset-top="85">
                        <ul class="nav nav-pills nav-stacked">
                            <!-- main menu links -->
                            <!-- HTML start -->
                            <li><a href="index.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 1. 23.01</a>
                            </li>
                            <li><a href="html01hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 1.</a></li>
                            <li><a href="html02.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 2</a></li>
                            <li><a href="html02hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 2</a></li>
                            <li><a href="html03.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 3</a></li>
                            <li><a href="html03hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 3</a></li>
                            <li><a href="html04.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 4</a></li>
                            <li><a href="html04hw.html"><span class="glyphicon glyphicon-home"></span>HTML5. Занятие 4. ДЗ</a></li>
                            <li><a href="html05.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 5</a></li>
                            <!-- HTML end -->
                            <li class="divider">
                                <hr>
                            </li>
                            <!-- PHP start -->
                            <li><a href="php02.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 2+ДЗ. 24.01</a></li>
                            <li><a href="php03.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 3 + ДЗ.</a>
                            <li><a href="php04.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 4. Начало ООП</a></li>
                            <li><a href="php05.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 5. Продолжение ООП</a></li>
                            <li><a href="php05hw-forms.php"><span class="glyphicon glyphicon-home"></span> PHP. Занятие 5. ДЗ классы элементов форм</a></li>
                            <li><a href="php06.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 6. Работа с БД</a></li>
                            <li class="active"><a href="php06hw-sql.php"><span class="glyphicon glyphicon-home"></span> PHP.
                                    Занятие 6. Работа с БД - ДЗ</a></li>
                            <!-- PHP end -->
                            <li class="divider">
                                <hr>
                            </li>
                            <li><a href="tutorial01.html">Tutorial. Shuffle Letters</a></li>
                            <!-- /main menu links -->
                        </ul>
                    </div>
                </div>

                <!-- login / registration modals -->
                <?php if (!$_SESSION['userlogged'] or ! isset($_SESSION['userlogged'])): ?>
                    <!------------------------------------------------------------------------------------ modal forms start -->
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('#login-modal').on('shown.bs.modal', function () {
                                $('#email').focus()
                            })
                        })
                    </script>

                    <!-- login modal form -->
                    <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="LoginForm"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="LoginForm">Авторизация</h4>
                                </div>

                                <form class="form-horizontal" action="php06hw-sql.php" method="post">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="email" class="control-label col-lg-3">E-mail </label>

                                            <div class="col-lg-9">
                                                <input type="email" id="email" name="email" class="form-control"
                                                       placeholder="e-mail">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="password" class="control-label col-lg-3">Пароль </label>

                                            <div class="col-lg-9">
                                                <input type="password" id="password" name="password" class="form-control"
                                                       placeholder="пароль">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" id='login-btn' name="login-btn" class="btn btn-info">Войти
                                        </button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                                        <button type="submit" id="restore-pass-btn" name="restore-pass-btn"
                                                class="btn btn-link">Забыли пароль?
                                        </button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>

                    <!-- registration modal form -->
                    <div class="modal fade" id="registration-modal" tabindex="-1" role="dialog" aria-labelledby="RegisterForm"
                         aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="RegisterForm">Регистрация</h4>
                                </div>
                                <span class="help-block col-lg-offset-2">Пароль должен содержать не менее 4 символов.<br />
                                    <i>Допускаются: 1-0 a-z A-Z _</i></span>
                                <form class="form-horizontal" action="php06hw-sql.php" method="post">
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="regname" class="control-label col-lg-3">Ваше имя </label>

                                            <div class="col-lg-9">
                                                <input type="text" id="regname" name="regname" class="form-control"
                                                       placeholder="Ваше имя">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="regemail" class="control-label col-lg-3">E-mail </label>

                                            <div class="col-lg-9">
                                                <input type="email" id="regemail" name="regemail" class="form-control"
                                                       placeholder="e-mail">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="regpassword" class="control-label col-lg-3">Пароль </label>

                                            <div class="col-lg-9">
                                                <input type="password" id="regpassword" name="regpassword" class="form-control"
                                                       placeholder="пароль">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" id="registration-btn" name="registration-btn"
                                                class="btn btn-info">Зарегистрироваться
                                        </button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Отмена</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <!-------------------------------------------------------------------------------------- modal forms end -->
                <?php endif; ?>

                <!--------------------------------------------------------------------------------------- modal for new user -->
                <?php if ($_GET['new_user'] == true && $_SESSION['new_user'] == true) : ?>
                    <?php $_SESSION['new_user'] = false; ?>
                    <script type="text/javascript">
                        $(document).ready(function () {
                            $('#newusermodal').modal('show');
                        })
                    </script>

                    <div class="modal fade" id="newusermodal">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Добро пожаловать!</h4>
                                </div>
                                <div class="modal-body">
                                    <p><?php echo $_SESSION['username'] . ', ' ?>добро пожаловать!<br />вы можете зайти в свою учетную запись.</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Ок</button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                <?php endif; ?>


                <div class="col-sm-12  col-lg-8">
                    <section class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title"> double submit problem</h3>
                        </div>
                        <div class="panel-body">
                            <p><a href="http://habrahabr.ru/post/86258/">Редирект после POST запроса</a></p>
                            <p><a href="http://stackoverflow.com/questions/7082063/double-submission-when-refresh-php">double submission when refresh php / Stack Overflow</a></p>
                        </div>
                    </section>
                    <section class="panel panel-primary">
                        <div class="panel-heading">
                            <h3 class="panel-title">Администрирование БД</h3>
                        </div>
                        <div class="panel-body">
                            <?php if ($_SESSION['userlogged'] == 1) : ?>
                                <?php if(($currentUser->getTypeName())=='administrator') :?>
                                <!----------------------------------------------------------------------------------- admin panel-->
                                    <section class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">Создать нового пользователя</h3>
                                        </div>
                                        <div class="panel-body">
                                            <?php
                                            if (isset($_POST['new-user-create'])){
                                                unset($_POST['new-user-create']);
                                                User::createUser($dbconnect, $_POST['new-user-name'],$_POST['new-user-email'],$_POST['new-user-password']);

                                                //сброс $_POST
                                                unset($_POST);
                                            }
                                            ?>
                                            <form  class="form-horizontal" name="create-new-user" action="php06hw-sql.php" method="post">
                                                <fieldset>
                                                    <legend>Информация о новом пользователе</legend>
                                                    <div class="form-group" >
                                                        <label for="new-user-name" class="col-lg-2 control-label">Фамилия</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" id="new-user-name" name="new-user-name" placeholder="Имя" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="new-user-email" class="col-lg-2 control-label">Email</label>
                                                        <div class="col-lg-10">
                                                            <input type="email" id="new-user-email" name="new-user-email" placeholder="Email" class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <label for="new-user-password" class="col-lg-2 control-label"> Пароль</label>
                                                        <div class="col-lg-10">
                                                            <input type="text" id="new-user-password" name="new-user-password" placeholder="Пароль" class="form-control">
                                                        </div>
                                                    </div>
                                                </fieldset>
                                                <button class="btn btn-danger pull-right" type="submit" name="new-user-create" id="new-user-create"><span class="glyphicon glyphicon-plus-sign"> </span> Добавить</button>
                                            </form>
                                        </div>
                                    </section>
                                <section class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Управление пользователями</h3>
                                    </div>
                                    <div class="panel-body">
                                        <form name="user-manage" action="php06hw-sql.php" method="post">
                                        <table class="table">
                                            <tr>
                                                <th>Тип</th>
                                                <th>Имя</th>
                                                <th>e-mail</th>
                                                <th>Управление</th>
                                            </tr>
                                            <?php
                                            //--------------------------------------Проведение операций с пользователями
                                            if (isset($_POST['user-manage-submit'])){
                                                unset($_POST['user-manage-submit']);
                                                while(list($userid,$new_type)=each($_POST)){
                                                    echo '<p><span class="label label-info">'. $new_type . ' user id-' . substr($userid, 3) . '</span></p>';
                                                    switch($new_type){
                                                        case 'activate':echo User::changeUserType($dbconnect,substr($userid, 3),2); break;
                                                        case 'deactivate':echo User::changeUserType($dbconnect,substr($userid, 3),3);
                                                    }
                                                }

                                                //сброс $_POST
                                                unset($_POST);
                                            }

                                            //в двумерный массив из БД считываются данные о пользователях
                                            $userlist=$dbconnect->getData('user_id, name, email, user_type', 'users');
                                            for($i=0;$i<count($userlist['name']);$i++){
                                                //формируеся таблица и управление пользователями
                                                $manage='';
                                                $tdclass='success';
                                                $trclass='admin-user';
                                                $status='<span class="glyphicon glyphicon-wrench" data-toggle="tooltip" data-placement="top" title="Администратор"> </span>';

                                                //формируем иконки
                                                if ($userlist['user_type'][$i]==2) {
                                                    $tdclass='info';
                                                    $trclass='member-user';
                                                    $status='<span class="glyphicon glyphicon-ok-circle" data-toggle="tooltip" data-placement="top" title="Пользователь"> </span>';
                                                }
                                                if ($userlist['user_type'][$i]==3) {
                                                    $tdclass='warning';
                                                    $trclass='inactive-user';
                                                    $status='<span class="glyphicon glyphicon-ban-circle" data-toggle="tooltip" data-placement="top" title="Заблокирован"> </span>';
                                                }

                                                echo '<tr class="'. $trclass .'">' . "\n";
                                                //иконки для разных типов
                                                echo '<td class="'. $tdclass .'">' . $status . '</td>' . "\n";
                                                echo '<td>' . $userlist['name'][$i] . '</td>' . "\n";
                                                echo '<td>' . $userlist['email'][$i] . '</td>' . "\n";

                                                //формируем управляющие кнопки
                                                if ($userlist['user_type'][$i]==2){
                                                    $manage_button=new Checkbox('id-'.$userlist['user_id'][$i].'-manage',
                                                        'id-'.$userlist['user_id'][$i],
                                                        'deactivate',
                                                        'Отключить');
                                                    $manage=$manage_button->render();
                                                } elseif($userlist['user_type'][$i]==3){
                                                    $manage_button=new Checkbox('id-'.$userlist['user_id'][$i].'-manage',
                                                        'id-'.$userlist['user_id'][$i],
                                                        'activate',
                                                        'Включить');
                                                    $manage=$manage_button->render();
                                                }
                                                echo '<td>' . $manage . '</tr>' . "\n";
                                                echo '</tr>' . "\n";
                                            }
                                            ?>
                                        </table>
                                            <button class="btn btn-danger pull-right" type="submit" name="user-manage-submit" id="user-manage-submit"><span class="glyphicon glyphicon-wrench"> </span> Применить</button>
                                        </form>
                                    </div>
                                </section>
                                <?php endif; ?>
                            <?php endif; ?>

                            <?php
                            //==================================================================================================
                            $dbconnect->showTablesList();
                            $tables = $dbconnect->getTableList();
                            echo '<pre>';
                            echo '$_SESSION'."\n";
                            print_r($_SESSION);

                            echo '$_POST'."\n";
                            print_r($_POST);
                            echo '</pre>';

                            function renderSelectList($inputID, $list) {
                                $selectelem = new Select($inputID, $inputID);
                                foreach ($list as $key => $value) {
                                    $option = new Option($value, $value);
                                    $selectelem->addOption($option);
                                }
                                $selectelem->setCSSClass('form-control');
                                echo $selectelem->render();
                            }

                            renderSelectList('basetables', $tables);
                            //==================================================================================================
                            ?>
                        </div>
                    </section>
                </div>
            </div>

            <hr>

            <footer>
                <p>&copy; Company 2014</p>
            </footer>
        </div>
        <!-- /container -->
    </body>
</html>
