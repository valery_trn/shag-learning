<?php
header("Content-Type: text/html; charset=utf-8");
?>
<!DOCTYPE html>
<!--[if lt IE 7]>
<html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>
<html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>
<html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js"> <!--<![endif]-->
<!--[if lt IE 9]>
<script src="js/vendor/html5shiv.js"></script>
<script src="js/vendor/respond.min.js"></script>
<![endif]-->
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>PHP. Продолжение ООП</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/bootstrap-theme.css">
    <link rel="stylesheet" href="css/custom-styles.css">
    <link rel="stylesheet" href="css/main.css">
    <script src="js/vendor/jquery-1.11.2.js" type="text/javascript"></script>
    <script src="js/vendor/bootstrap.js" type="text/javascript"></script>
    <script src="js/vendor/modernizr-2.6.2-respond-1.1.0.min.js" type="text/javascript"></script>
</head>
<body>
<!--[if lt IE 7]>
<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->
<div class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container">
        <div class="navbar-header">
            <h2>Занятия - часть 2. PHP. 5. Продолжение ООП</h2>
        </div>
    </div>
</div>

<div class="container main">
    <div class="row">
        <div class="col-sm-12  col-lg-4">
            <div class="panel panel-primary panel-body left-menu" data-spy="affix" data-offset-top="85">
                <ul class="nav nav-pills nav-stacked">
                    <!-- main menu links -->
                    <!-- HTML start -->
                    <li><a href="index.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 1. 23.01</a></li>
                    <li><a href="html01hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 1.</a></li>
                    <li><a href="html02.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 2</a></li>
                    <li><a href="html02hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 2</a></li>
                    <li><a href="html03.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 3</a></li>
                    <li><a href="html03hw.html"><span class="glyphicon glyphicon-home"></span> HTML5. ДЗ 3</a></li>
                    <li><a href="html04.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 4</a></li>
                    <li><a href="html04hw.html"><span class="glyphicon glyphicon-home"></span>HTML5. Занятие 4. ДЗ</a></li>
                    <li><a href="html05.html"><span class="glyphicon glyphicon-book"></span> HTML5. Занятие 5</a></li>
                    <!-- HTML end -->
                    <li class="divider"><hr></li>
                    <!-- PHP start -->
                    <li><a href="php02.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 2+ДЗ. 24.01</a></li>
                    <li><a href="php03.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 3 + ДЗ.</a>
                    <li><a href="php04.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 4. Начало ООП</a></li>
                    <li><a href="php05.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 5. Продолжение ООП</a></li>
                    <li class="active"><a href="php05hw-forms.php"><span class="glyphicon glyphicon-home"></span> PHP. Занятие 5. ДЗ классы элементов форм</a></li>
                    <li><a href="php06.php"><span class="glyphicon glyphicon-book"></span> PHP. Занятие 6. Работа с БД</a></li>
                    <li><a href="php06hw-sql.php"><span class="glyphicon glyphicon-home"></span> PHP. Занятие 6. Работа с БД - ДЗ</a></li>
                    <!-- PHP end -->
                    <li class="divider"><hr></li>
                    <li><a href="tutorial01.html">Tutorial. Shuffle Letters</a></li>
                    <!-- /main menu links -->
                </ul>
            </div>
        </div>
        <div class="col-sm-12  col-lg-8">
            <section class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">Форма!</h3>
                </div>
                <div class="panel-body">
                    <!--- panel-1 form-1 --->
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <?php
                            //spl_autoload_register подключает все файлы из указанной папки
                            spl_autoload_register(function ($class) {
                                include './class/form/' . $class . '.php';
                            });

                            //create new form and add atributes
                            $myform = new Form();
                            $myform->setFormMethod('POST');
                            $myform->setFormAction('php05hw-forms.php');
                            $myform->setCSSClass('form-horizontal col-lg-8 col-lg-offset-2');

                            //create new elements to form
                            //
                            //---new button---
                            $button = new Input('btn', 'btn');
                            $button->setType('submit');
                            $button->setCSSClass('btn btn-success');
                            $button->setValue('Готово');

                            //---new text---
                            $textelem = new InputText('username', 'username', 'Enter your name', 50);
                            $textelem->setCSSClass('form-control');
                            $textelem->setAdditional('data-toggle="tooltip" data-placement="top" title="Enter your name"');
                            $textelem->disable();

                            $email = new InputText('email', 'email', 'Enter your email');
                            $email->setCSSClass('form-control');
                            $email->setType('email');
                            $email->setAdditional('data-toggle="tooltip" data-placement="top" title="Enter your email"');

                            //---label---
                            $namelabel = new Label();
                            $namelabel->setText('Name:');
                            $textelem->addLabel($namelabel);

                            $maillabel = clone $namelabel;
                            $maillabel->setText('E-mail:');
                            $email->addLabel($maillabel);

                            //---textarea---
                            $message = new Textarea('message', 'message', 4, 58, 'Enter your message');

                            //---radiobuttons--
                            $radio1 = new Radiobutton('', 'number', 'first', 'First button');
                            $radio2 = new Radiobutton('', 'number', 'second', 'Second button');
                            $radio2->check();
                            $radio3 = new Radiobutton('', 'number', 'Third', 'Third');


                            //---array of form elements--
                            $formelems = array(
                                'fg1' => '<div class="form-group">',
                                'nameinput' => $textelem->render(),
                                'fg1end' => '</div>',
                                'fg2' => '<div class="form-group">',
                                'emailinput' => $email->render(),
                                'fg2end' => '</div>'
                            );

                            $myform->addFormElement($formelems);
                            $myform->addFormElement(array(
                                    $message->render(),
                                    '<div class="form-group">',
                                    $radio1->render(),
                                    $radio2->render(),
                                    $radio3->render(),
                                    '</div>',
                                    $button->render()
                                )
                            );


                            //display form
                            echo $myform->render();
                            ?>
                        </div>
                    </div>
                    <!--- /form-1 /panel-1 --->

                    <!---  panel-2 form-2 --->
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <form class="col-lg-12" name="parameters" id="parameters" action="php05hw-forms.php" method="post">
                                <fieldset>
                                    <legend>Parameters:</legend>
                                    <?php
                                    //add 3 input elements into html-wrappers
                                    $xparam = new InputText('xparam', 'xparam', 'Parameter X', 10);
                                    $yparam = new InputText('yparam', 'yparam', 'Parameter Y', 10);
                                    $zparam = new InputText('zparam', 'zparam', 'Parameter Z', 10);
                                    $xparam->setCSSClass('form-control');
                                    $yparam->setCSSClass('form-control');
                                    $zparam->setCSSClass('form-control');
                                    ?>
                                    <div class="thumbnail clearfix">
                                        <div class="col-lg-4">
                                            <div class="input-group">
                                                <?php echo $xparam->render(); ?>
                                                <span class="input-group-addon">px</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="input-group">
                                                <?php echo $yparam->render(); ?>
                                                <span class="input-group-addon">px</span>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="input-group">
                                                <?php echo $zparam->render(); ?>
                                                <span class="input-group-addon">px</span>
                                            </div>
                                        </div>
                                        <label class="checkbox-inline">
                                            <?php
                                            $chooseparam1=new Checkbox('chooseparam1','chooseparam1','param1','Parameter 1');
                                            echo $chooseparam1->render();
                                            ?>
                                        </label>
                                        <label class="checkbox-inline">
                                            <?php
                                            $chooseparam2=new Checkbox('chooseparam2','chooseparam2','param2','Parameter 2');
                                            $chooseparam2->check();
                                            echo $chooseparam2->render();
                                            ?>
                                        </label>

                                        <?php
                                        //add select element
                                        $select=new Select('options','options');
                                        $select->setCSSClass('form-control');
                                        $optionbig=new Option('big','Big');
                                        $select->addOption($optionbig);
                                        $select->createOption('medium','Medium');
                                        $select->createOption('small','Small');
                                        $select->render();
                                        
                                        ?>
                                        <div class='col-lg-6'>
                                            <div class='form-group'>
                                                <?php 
                                                //add number elements
                                                $newparam1=new InputNumber('newparam1', 'newparam1');
                                                $newparam1->setCSSClass('form-control');
                                                $np1lbl=new Label('', '', 'input number');
                                                $newparam1->addLabel($np1lbl);
                                                $newparam1->setMinMaxStep(5, 40, 5);
                                                $newparam1->setValue(35);
                                                echo $newparam1->render();
                                                ?>                                                
                                            </div>
                                        </div>
                                        <div class='col-lg-6'>
                                            <div class='form-group'>
                                                <?php 
                                                $newparam2=new InputNumber('newparam2', 'newparam2');
                                                $newparam2->setCSSClass('form-control');
                                                $np2lbl=new Label('', '', 'Input another number');
                                                $newparam2->addLabel($np2lbl);
                                                echo $newparam2->render();
                                                ?>                                                
                                            </div>
                                        </div>
                                        <?php 
                                        //add buttons
                                        $okbtn=new Button('okbtn', 'okbtn', 'submit','Ok');
                                        $okbtn->setCSSClass('btn btn-primary');
                                        echo $okbtn->render();
                                        $resbtn=new Button('res','res','reset','Reset');
                                        $resbtn->setAdditional(' style="margin-left:10px;" ');
                                        $resbtn->setCSSClass('btn btn-info');
                                        echo $resbtn->render();
                                        ?>
                                        </div>

                                    <span id="helpBlock" class="help-block">Form created by php OOP</span>
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>

            </section>
        </div>
    </div>

    <hr>

    <footer>
        <p>&copy; Company 2014</p>
    </footer>
</div>
<!-- /container -->
<script type="application/javascript">
    $(function () {
        $('[data-toggle="tooltip"]').tooltip(

        );

    })
</script>
</body>
</html>



